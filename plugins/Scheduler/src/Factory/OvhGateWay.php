<?php

namespace Scheduler\Factory;

use Ovh\Api;

class OvhGateWay implements IGateway
{
    // https://eu.api.ovh.com/createToken/index.cgi?GET=/sms&GET=/sms/*/jobs&POST=/sms/*/jobs
    private $applicationKey = '';
    private $applicationSecret = '';
    private $consumerKey = '';
    private $endPoint = '';

    public function __construct(object $config)
    {
        $this->applicationKey = $config->applicationKey;
        $this->applicationSecret = $config->applicationSecret;
        $this->consumerKey = $config->consumerKey;
        $this->endPoint = $config->endPoint;
    }

    function execute($to, $body): bool
    {

        $ovhClient = new Api(
            $this->applicationKey,
            $this->applicationSecret,
            $this->endPoint,
            $this->consumerKey
        );

        $services = $ovhClient->get('/sms/');

        $content = (object)array(
            "charset" => "UTF-8",
            "class" => "phoneDisplay",
            "coding" => "7bit",
            "message" => $body,
            "noStopClause" => false,
            "priority" => "high",
            "receivers" => [$to],
            "senderForResponse" => true,
            "validityPeriod" => 2880
        );

        $response = $ovhClient->post('/sms/' . $services[0] . '/jobs', $content);

        return !empty($response);

    }
}
