<?php

namespace Scheduler\Factory;

use Cake\ORM\TableRegistry;

class MessageGatewayFactory
{

    public static function buildDefaultGateway()
    {
        // Retrieve the default gateway config
        $config = TableRegistry::getTableLocator()
            ->get('Admin.Gateways')
            ->find()
            ->where(['selected' => 1])
            ->first();

        $parameters = json_decode($config->config);

        return $config->code === 'OVH' ?
            new OvhGateWay($parameters) : new TwilioGateWay($parameters);

    }

    public static function getDefaultGatewayId()
    {
        // Retrieve the default gateway config
        $query = TableRegistry::getTableLocator()
            ->get('Admin.Gateways')
            ->find()
            ->where(['selected' => 1])
            ->first();

        return $query->id;

    }



}
