<?php

namespace Scheduler\Factory;


use Twilio\Rest\Client;

class TwilioGateWay implements IGateway
{
    private $mobileClearPatterns = ['(',')','-','_'];

    // Your Account SID and Auth Token from twilio.com/console
    private $sid = 'AC0938a0ff997ebf5673bd04f2beb9181d';
    private $token = 'dca6873beded5a69cb819f9b847e7c32';
    private $from = '+18305326952';
    private $client;

    public function __construct(object $config)
    {
        $this->sid =$config->sid;
        $this->token =$config->token;
        $this->from =$config->from;
        $this->client = new Client($this->sid, $this->token);
    }

    private function parsePhoneNumber(string $mobile): string
    {
        return array_reduce($this->mobileClearPatterns, function($carry, $pattern) {
            return trim( str_replace($pattern,'',$carry) );
        },$mobile);
    }


    function execute($to, $body)
    {
        // Use the client to do fun stuff like send text messages!
        return $this->client->messages->create(
            $this->parsePhoneNumber($to),
            [
                // A Twilio phone number you purchased at twilio.com/console
                'from' => $this->parsePhoneNumber($this->from),
                // the body of the text message you'd like to send
                'body' => $body
            ]
        );

    }
}
