<?php

namespace Scheduler\Factory;

interface IGateway
{
    function execute($to, $body);
}
