<?php

namespace Scheduler\Command;

use Cake\Console\Arguments;
use Cake\Console\Command;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\ORM\TableRegistry;
use Scheduler\Factory;

/**
 * DispatchMessage command.
 */
class DispatchMessageCommand extends Command
{
    /**
     * Hook method for defining this command's option parser.
     *
     * @see https://book.cakephp.org/3.0/en/console-and-shells/commands.html#defining-arguments-and-options
     *
     * @param \Cake\Console\ConsoleOptionParser $parser The parser to be defined
     * @return \Cake\Console\ConsoleOptionParser The built parser.
     */
    public function buildOptionParser(ConsoleOptionParser $parser)
    {
        $parser = parent::buildOptionParser($parser);

        return $parser;
    }

    /**
     * Implement this method with your command's logic.
     *
     * @param \Cake\Console\Arguments $args The command arguments.
     * @param \Cake\Console\ConsoleIo $io The console io
     * @return null|int The exit code or null for success
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        // Retrieve the default active gateway
        $gatewayClient = Factory\MessageGatewayFactory::buildDefaultGateway();

        $model = TableRegistry::getTableLocator()
            ->get('Admin.Schedules');

        // Extract pending Messages
        $query = $model
            ->find('all')
            ->where(['status' => 'pending'])
            ->contain(['Contacts']);

        // Process the queue
        foreach ($query as $message) {

            try {

                $mobile = $message->contact->mobile_phone;

                if (strpos($mobile, "+33") !== 0) {
                    $mobile = "+33" . $mobile;
                }

                $result = $gatewayClient->execute($mobile, $message->sms_body);

                // Update Schedule status
                $status = $result ? 'sent' : 'failed';
                $model->updateAll([
                    'status' => $status,
                    'gateway_id' => Factory\MessageGatewayFactory::getDefaultGatewayId()
                ], ['id' => $message->id]);

                // Log out the message processing status
                $io->out("Message #{$message->id} => $status");


            } catch (\Exception $e) {

                $model->updateAll([
                    'status' => 'failed',
                    'gateway_id' => Factory\MessageGatewayFactory::getDefaultGatewayId()
                ], ['id' => $message->id]);

                $io->out("Message #{$message->id} => failed");

            }

        }

        $io->out('Schedule Queue proceed');
    }
}
