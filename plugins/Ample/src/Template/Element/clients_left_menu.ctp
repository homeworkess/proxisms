<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav slimscrollsidebar">
        <div class="sidebar-head">
            <h3><span class="fa-fw open-close"><i class="ti-close ti-menu"></i></span> <span class="hide-menu">Navigation</span></h3> </div>
        <?= $this->element('profile') ?>
        <ul class="nav" id="side-menu">

            <li>
                <a  class="active" href="<?= $this->url->build("/agency/dashboard") ?>">
                    <i class="mdi mdi-chart-bar fa-fw"></i><span class="hide-menu">Tableau de Bord</span>
                </a>
            </li>
            <li>
                <a href="javascript:void(0)" class="waves-effect"><i class="mdi mdi-apps fa-fw"></i> <span class="hide-menu">Mes Applications<span class="fa arrow"></span></span></a>
                <ul class="nav nav-second-level">

                    <li><a href="<?= $this->url->build("/agency/clients") ?>"><i class="ti-user fa-fw"></i><span class="hide-menu">Clients</span></a></li>
                    <li><a href="<?= $this->url->build("/agency/estates") ?>"><i class="ti-user fa-fw"></i><span class="hide-menu">Biens immobiliers</span></a></li>
                    <li><a href="<?= $this->url->build("/agency/goods") ?>"><i class="ti-user fa-fw"></i><span class="hide-menu">Rendez-vous</span></a></li>
                    <li><a href="<?= $this->url->build("/agency/goods") ?>"><i class="mdi mdi-clipboard-text fa-fw"></i><span class="hide-menu">Mes notes</span></a></li>

                </ul>
            </li>
            <li class="devider"></li>
            <li><a href="<?= $this->url->build("/agency/goods") ?>"><i class="mdi mdi-logout fa-fw"></i><span class="hide-menu">Déconnexion</span></a></li>
        </ul>
    </div>
</div>