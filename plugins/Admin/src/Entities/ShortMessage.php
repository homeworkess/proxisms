<?php


class ShortMessage
{
    private $from;
    private $to;
    private $body;

    private function __construct($from,$to,$body)
    {
        $this->from = $from;
        $this->to = $to;
        $this->body = $body;
    }

    public static function build($params){

        if(empty($params['to'])){
            throw new Exception('Le destinataire ne peut être vide');
        }

        return new ShortMessage(
            $params['from'],
            $params['to'],
            $params['body']
        );

    }

}
