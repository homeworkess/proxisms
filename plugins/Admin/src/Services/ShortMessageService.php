<?php


namespace Admin\Services;


use Admin\Repositories\ShortMessageRepository;
use Admin\UseCase\SendMessage;
use Cake\ORM\TableRegistry;

class ShortMessageService
{
    private $configService;
    private $userId;

    public function __construct(int $userId)
    {
        $this->configService = new ConfigService();
        $this->userId = $userId;
    }

    private function parseMessageBody(string $message): string
    {
        return "Parsed";
    }

    private function fetchTemplateMessageBody(int $templateId): string
    {
        return TableRegistry::getTableLocator()
            ->get('Admin.Templates')
            ->get($templateId)
            ->body;
    }

    public function sendBatchMessages(array $ids, int $templateId)
    {
        if (empty($ids)) throw new \Exception('You should provide at least one id');

        if (is_null($templateId)) throw new \Exception('You must provide a template');

        // Retrieve the default SMS template
        $messageBodyTemplate = $this->fetchTemplateMessageBody($templateId);

        // Return the Dtos
        return array_map(function($contactId) use($messageBodyTemplate){

            $sendMessage = new SendMessage(
                new ShortMessageRepository(),
                $contactId,
                $messageBodyTemplate,
                $this->userId
            );

            return $sendMessage->execute();

        },$ids);

    }

}
