<?php

namespace Admin\Repositories;

use Admin\Model\Entity\Schedule;
use Cake\ORM\TableRegistry;

class ShortMessageRepository
{
    public function createMessage($contactId, $messageBody, $userId)
    {
        $model = TableRegistry::getTableLocator()
            ->get('Admin.Schedules');

        $message = $model->newEntity();

        $model->patchEntity($message,[
            'contact_id' => $contactId,
            'user_id' => $userId,
            'sms_body' => $messageBody,
            'status' => 'pending'
        ]);

        $model->save($message);

        return $message;
    }

}
