<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav">
        <div class="sidebar-head">
            <h3><span class="fa-fw open-close"><i class="ti-close ti-menu"></i></span> <span class="hide-menu"></span>
            </h3></div>
        <ul class="nav" id="side-menu">
            <li>
                <a href="<?= $this->Url->build("/admin/dashboard") ?>">
                    <i class="mdi mdi-chart-bar fa-fw"></i><span class="hide-menu">TABLEAU DE BORD</span>
                </a>
            </li>
            <li class="devider"></li>

            <li>
                <a href="<?= $this->Url->build(['controller' => 'Contacts', 'action' => 'index']) ?>">
                    <i class="mdi mdi-contacts"></i><span class="hide-menu"> CONTACTS</span>
                </a>
            </li>

            <li>
                <a href="<?= $this->Url->build(['controller' => 'Templates', 'action' => 'index']) ?>">
                    <i class="mdi mdi-message"></i><span class="hide-menu"> TEMPLATES SMS</span>
                </a>
            </li>

            <li>
                <a href="<?= $this->Url->build(['controller' => 'History', 'action' => 'index']) ?>">
                    <i class="mdi mdi-history"></i><span class="hide-menu"> HISTORIQUE</span>
                </a>
            </li>

            <?php

                if($this->request->session()->read('Auth.Admin.group_id') === 2) { ?>

                    <li>
                        <a href="<?= $this->Url->build(['controller' => 'Users', 'action' => 'index']) ?>">
                            <i class="mdi mdi-account"></i><span class="hide-menu"> UTILISATEURS</span>
                        </a>
                    </li>

                    <li>
                        <a href="<?= $this->Url->build(['controller' => 'Gateways', 'action' => 'index']) ?>">
                            <i class="mdi mdi-settings"></i><span class="hide-menu"> PASSERELLES</span>
                        </a>
                    </li>

                <?php }

            ?>

            <li class="devider"></li>
            <li>
                <a href="<?= $this->Url->build(['plugin' => 'authentication', 'controller' => 'users', 'action' => 'logout']) ?>"><i
                        class="mdi mdi-logout fa-fw"></i><span class="hide-menu">DECONNEXION</span></a></li>
        </ul>
    </div>
</div>
