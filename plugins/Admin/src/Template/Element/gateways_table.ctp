<div class="filter-container">

    <div class="right-content ">

        <div style="flex-basis: 200px !important;">

        </div>

    </div>


</div>

<div class="row">
    <div class="col-sm-12">
        <div class="table-responsive">
            <table id="contacts" class="table table-striped">
                <thead>
                <tr>
                    <th>ID</th>
                    <th class="text-right">Provider</th>
                    <th class="text-right">Status</th>
                    <th class="text-right">Par défaut</th>
                    <th class="text-right">ACTIONS</th>
                </tr>
                </thead>
                <tbody>

                <?php foreach ($data as $d): ?>
                    <tr>
                        <td><?= $d->id ?></td>
                        <td class="text-right">
                            <a href="javascript:void(0)">
                                <?= h(ucfirst($d->name)) ?>
                            </a>
                        </td>

                        <td class="text-right">
                            <span class="badge <?= $d->active ? 'badge-success' : 'badge-danger' ?>">
                                <?= $d->active ? 'Active' : 'Inactive' ?>
                            </span>
                        </td>

                        <td class="text-right">
                            <span class="badge <?= $d->selected ? 'badge-success' : 'badge-danger' ?>">
                                <?= $d->selected ? 'Oui' : 'Non' ?>
                            </span>
                        </td>

                        <td class="text-right">
                            <a href="<?= $this->Url->build(['controller' => 'Gateways', 'action' => 'edit', $d->id]) ?>"
                               class="btn btn-default btn-sm br-round">EDITER</a>
                        </td>

                    </tr>
                <?php endforeach; ?>

                </tbody>
            </table>


        </div>
    </div>
</div>


<script type="text/javascript">

    $(document).ready(function () {

        const contactsTable = $('#contacts').DataTable({
            dom: 'Bfrtip',
            searching: true,

            columnDefs: [
                {
                    "targets": [0],
                    "visible": false,
                    "searchable": true
                },
                {
                    "targets": [1],
                    "visible": true,
                    "searchable": true,
                    "orderable": false
                }
            ],

            "pageLength": 100,

            order: [[0, "desc"]],

            language: {
                search: "",
                searchPlaceholder: "Recherche ...",
                processing: "Traitement en cours...",
                lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
                info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                infoPostFix: "",
                loadingRecords: "Chargement en cours...",
                zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
                emptyTable: "Aucune donnée disponible dans le tableau",
                paginate:
                    {
                        next: "Suivant",
                        previous: "Précédent",
                        entries: "Entrées",
                        showing: "Affichage",
                    }
            },

            "footerCallback": function (row, data, start, end, display) {
            }


        });
        const endpoint = "<?= $this->Url->build(['controller' => 'Contacts', 'action' => 'sendMessage']) ?>";

        const clearSelectionList = () => {
            selectedContacts = [];
        };

        const toggleContactSelection = () => {
            // Toggle the selection status
            currentGroupSelection = !currentGroupSelection;
            $('.contact-item').prop('checked', currentGroupSelection);

            // Clear the contact selection
            clearSelectionList();

            // Update contact selection
            $('.contact-item').each(function(index) {
                if(currentGroupSelection){
                    selectedContacts.push( $(this).data('id') );
                }
            });
        };

        const unselectAllContacts = () => {
            $('.contact-item').prop('checked', false);
            $('.contact-select-all').prop('checked', false);
            clearSelectionList();
        }

        // Request the api
        const sendMessage = async (ids) => {

            const request = await fetch(endpoint + '?ids=' + contactSelectionFormat(ids))
            const result = await request.json();

            const responseMsg = result.results.success ? 'Messages envoyés' : 'Une erreur est survenue';
            alert(responseMsg);
        }

        let selectedContacts = [];
        let currentGroupSelection = false;

        const contactSelectionFormat = (params) => params.join(',');

        // Hide the default search input
        $('#contacts_filter').hide();

        // Single message sending btn
        $('.contact-send-msg').on('click',async function(){
            const ids = [];
            ids.push([].push( $(this).data('id') ) );
            await sendMessage(ids);
        });

        // Click sur les lignes
        $('.contact-item').on('click', function () {

            const id = $(this).data('id');

            if (this.checked)
                selectedContacts.push(id);
            else
                selectedContacts = selectedContacts.filter(function (item) {
                    return item !== id;
                })

        });

        // Group selection
        $('.contact-select-all').on('click', () => toggleContactSelection());

        // Send message button action
        $('#send-group-btn').on('click', async function () {

            // Check if the selection is not empty
            if(selectedContacts.length === 0){
                alert('Selectionnez au minimum un contact');
                return;
            }

            // Request the API
            await sendMessage(selectedContacts);

            unselectAllContacts();
        });

    });

</script>

<style type="text/css">

    input[type="search"] {
        padding: 8px;
    }

    table.dataTable thead th, table.dataTable thead td {
        padding: 10px 18px;
        border-bottom: none;
    }

    .filter-container {

        display: flex;

    }

    .filter-container > div {
        flex: 1;
        margin: 0;
    }

    .right-content {

        display: flex;
        justify-content: flex-end;

    }

    .right-content div {
        margin-left: 1em;

    }

    .flex-large {
        width: 400px;
    }

    .br-round {
        border-radius: 25px;
    }

</style>
