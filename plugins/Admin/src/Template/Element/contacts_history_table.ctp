<div class="filter-container">


</div>

<div class="row">
    <div class="col-sm-12">
        <div class="table-responsive">
            <table id="contacts" class="table table-striped">
                <thead>
                <tr>
                    <th>ID</th>
                    <th class="text-right">Date</th>
                    <th class="text-right">Statut</th>
                    <th class="text-right">Numéro</th>
                    <th class="text-right">Envoyé par</th>
                    <th class="text-right">Passerelle</th>
                    <th class="text-right">Message</th>
                    <th class="text-right">Actions</th>
                </tr>
                </thead>
                <tbody>

                <?php foreach ($data as $d): ?>
                    <tr>
                        <td><?= $d->id ?></td>

                        <td class="text-right">
                            <?= $d->created->i18nFormat('dd/MM/yyyy h:mm:ss') ?>
                        </td>

                        <td class="text-right">
                            <a href="javascript:void(0)">
                                <?php

                                // Refactor into an UI Element
                                $element = '';
                                switch ($d->status) {
                                    case 'pending':
                                        $element = "<span class='badge badge-warning'>En attente</span>";
                                        break;

                                    case 'sent':
                                        $element = "<span class='badge badge-success'>Envoyé</span>";
                                        break;

                                    case 'failed':
                                        $element = "<span class='badge badge-danger'>Echoué</span>";
                                        break;

                                    case 'deleted':
                                        $element = "<span class='badge badge-danger'>Supprimé</span>";
                                        break;

                                }

                                echo $element;

                                ?>
                            </a>
                        </td>

                        <td class="text-right">
                            <?= $d->contact->mobile_phone ?>
                        </td>

                        <td class="text-right">
                            <?= $d->user ? $d->user->username : 'N/D' ?>
                        </td>

                        <td class="text-right">
                            <?=
                                $d->gateway ? $d->gateway->code : 'N/D'
                            ?>
                        </td>

                        <td class="text-right">
                            <?= $this->Text->excerpt($d->sms_body, 'method', 50, '...') ?>


                        <td class="text-right">
                            <?= $this->Html->link(
                                'Supprimer',
                                [
                                    'controller' => 'History', 'action' => 'delete', $d->id
                                ],
                                [
                                    'confirm' => 'êtes-vous sûr de vouloir supprimer le SMS ?',
                                    'class' => "btn btn-danger btn-sm br-round"
                                ]
                            ) ?>
                        </td>

                    </tr>
                <?php endforeach; ?>

                </tbody>
            </table>


        </div>
    </div>
</div>


<script type="text/javascript">

    $(document).ready(function () {

        const contactsTable = $('#contacts').DataTable({
            dom: 'Bfrtip',
            searching: true,

            columnDefs: [
                {
                    "targets": [0],
                    "visible": false,
                    "searchable": true
                },
                {
                    "targets": [1],
                    "visible": true,
                    "searchable": true,
                    "orderable": false
                }
            ],

            "pageLength": 100,

            order: [[0, "desc"]],

            language: {
                search: "",
                searchPlaceholder: "Recherche ...",
                processing: "Traitement en cours...",
                lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
                info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                infoPostFix: "",
                loadingRecords: "Chargement en cours...",
                zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
                emptyTable: "Aucune donnée disponible dans le tableau",
                paginate:
                    {
                        next: "Suivant",
                        previous: "Précédent",
                        entries: "Entrées",
                        showing: "Affichage",
                    }
            },

            "footerCallback": function (row, data, start, end, display) {
            }

        });

</script>

<style type="text/css">

    input[type="search"] {
        padding: 8px;
    }

    table.dataTable thead th, table.dataTable thead td {
        padding: 10px 18px;
        border-bottom: none;
    }

    .filter-container {

        display: flex;

    }

    .filter-container > div {
        flex: 1;
        margin: 0;
    }

    .right-content {

        display: flex;
        justify-content: flex-end;

    }

    .right-content div {
        margin-left: 1em;

    }

    .flex-large {
        width: 400px;
    }

    .br-round {
        border-radius: 25px;
    }

</style>
