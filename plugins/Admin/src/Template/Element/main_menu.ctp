<?php

$session = $this->request->session()->read('Auth.Admin');

?>

<nav class="navbar navbar-default navbar-static-top m-b-0">
    <div class="navbar-header">

        <div class="top-left-part">
            <!-- Logo -->
            <a class="logo" href="<?= $this->Url->build('/') ?>">
            </a>
        </div>

        <!-- /Logo -->
        <ul class="nav navbar-top-links navbar-left">
            <li><a href="javascript:void(0)" class="open-close waves-effect waves-light visible-xs">
                    <i class="ti-close ti-menu"></i></a></li>
        </ul>

        <ul class="nav navbar-top-links navbar-right pull-right">
            <li class="dropdown">
                <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#">
                    <strong></strong>
                </a>
            </li>
        </ul>

    </div>
</nav>
