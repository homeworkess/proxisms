<div class="filter-container">

    <div class="right-content ">

        <div style="flex-basis: 200px !important;">

        </div>

    </div>


</div>

<div class="row">
    <div class="col-sm-12">
        <div class="table-responsive">
            <table id="contacts" class="table table-striped">
                <thead>
                <tr>
                    <th>ID</th>
                    <th class="text-right">Titre</th>
                    <th class="text-right">Message</th>
                    <th class="text-right">Status</th>
                    <th class="text-right">Ordre d'affichage</th>
                    <th class="text-right">ACTIONS</th>
                </tr>
                </thead>
                <tbody>

                <?php foreach ($data as $d): ?>
                    <tr>
                        <td><?= $d->id ?></td>
                        <td class="text-right">
                            <?= h(ucfirst($d->name)) ?>
                        </td>

                        <td class="text-right">
                            <?= h(substr(ucfirst($d->body), 0, 60) . '...') ?>
                        </td>

                        <td class="text-right">
                            <span class="badge"><?= $d->active ? 'Actif' : 'Inactif' ?></span>
                        </td>

                        <td class="text-right">
                            <?= h($d->sequence) ?>
                        </td>

                        <td class="text-right">

                            <a href="<?= $this->Url->build(['controller' => 'Templates', 'action' => 'edit', $d->id]) ?>"
                               class="btn btn-default btn-sm br-round">EDITER</a>

                            <?= $this->Html->link(
                                'Supprimer',
                                [
                                    'controller' => 'Templates', 'action' => 'delete', $d->id
                                ],
                                [
                                    'confirm' => 'êtes-vous sûr de vouloir supprimer le template ?',
                                    'class' => "btn btn-danger btn-sm br-round"
                                ]
                            ) ?>


                        </td>

                    </tr>
                <?php endforeach; ?>

                </tbody>
            </table>


        </div>
    </div>
</div>


<script type="text/javascript">

    $(document).ready(function () {

        const contactsTable = $('#contacts').DataTable({
            dom: 'Bfrtip',
            searching: true,
            columnDefs: [
                {
                    "targets": [0],
                    "visible": true,
                    "searchable": true
                },
                {
                    "targets": [1],
                    "visible": true,
                    "searchable": true,
                    "orderable": false
                }
            ],

            "pageLength": 100,

            order: [[5, "asc"]],

            language: {
                search: "",
                searchPlaceholder: "Recherche ...",
                processing: "Traitement en cours...",
                lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
                info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                infoPostFix: "",
                loadingRecords: "Chargement en cours...",
                zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
                emptyTable: "Aucune donnée disponible dans le tableau",
                paginate:
                    {
                        next: "Suivant",
                        previous: "Précédent",
                        entries: "Entrées",
                        showing: "Affichage",
                    }
            },

            "footerCallback": function (row, data, start, end, display) {
            }


        });


        // Hide the default search input
        $('#contacts_filter').hide();

    });

</script>

<style type="text/css">

    input[type="search"] {
        padding: 8px;
    }

    table.dataTable thead th, table.dataTable thead td {
        padding: 10px 18px;
        border-bottom: none;
    }

    .filter-container {

        display: flex;

    }

    .filter-container > div {
        flex: 1;
        margin: 0;
    }

    .right-content {

        display: flex;
        justify-content: flex-end;

    }

    .right-content div {
        margin-left: 1em;

    }

    .flex-large {
        width: 400px;
    }

    .br-round {
        border-radius: 25px;
    }

</style>
