<?php

$tagsToHtml = function (string $flatTag) {
    $tags = explode(',', trim($flatTag));
    return array_map(function ($tag) {
        return "<span class='badge'>" . trim($tag) . "</span>";
    }, $tags);
};

//echo (new \Cake\Auth\DefaultPasswordHasher())->hash("utilisateur@proxidesk.fr");
//die;

?>


<div class="row">
    <div class="col-sm-4">
        <div class="input-group">
            <span class="input-group-btn">
                <button type="button" class="btn waves-effect btn-wj waves-light btn-info"><i class="fa fa-search"></i></button>
            </span>
            <input type="text" id="contacts_search" class="form-control" placeholder="Chercher ...">
        </div>
    </div>
    <div class="col-sm-5"></div>
    <div class="col-sm-3">
        <a href="javascript:void(0)" id="send-group-btn"
           class="btn btn-block btn-wj pull-right br-round">Envoyer un message</a>
    </div>
</div>


<div class="row">
    <div class="col-sm-12">
        <div class="table-responsive">
            <table id="contacts" class="table table-striped">
                <thead>
                <tr>
                    <th>ID</th>
                    <td class="text-left sales-order-all">
                        <input type="checkbox" class="contact-select-all"/>
                    </td>
                    <th class="text-right">Nom</th>
                    <th class="text-right">Prénom</th>
                    <th class="text-right">Mobile</th>
                    <th class="text-right">Adresse</th>
                    <th class="text-right">Tags</th>
                    <th class="text-right">ACTIONS</th>
                </tr>
                </thead>
                <tbody>

                <?php foreach ($data as $d): ?>
                    <tr>
                        <td><?= $d->id ?></td>
                        <td class="text-left  sales-order">
                            <input type="checkbox" class="contact-item" data-id="<?= $d->id ?>"/>
                        </td>
                        <td class="text-right">
                            <a href="javascript:void(0)">
                                <?= $d->first_name ?>
                            </a>
                        </td>

                        <td class="text-right">
                            <a href="javascript:void(0)">
                                <?= $d->last_name ?>
                            </a>
                        </td>

                        <td class="text-right">
                            <a href="javascript:void(0)">
                                <?= $d->mobile_phone ?>
                            </a>
                        </td>

                        <td class="text-right">
                            <a href="javascript:void(0)">
                                <?= $d->address ?>
                            </a>
                        </td>

                        <td class="text-right">
                            <?php foreach ($tagsToHtml($d->tags) as $htmlTag) {
                                echo $htmlTag;
                            } ?>
                        </td>

                        <td class="text-right">

                            <a href="<?= $this->Url->build(['controller' => 'Contacts', 'action' => 'history', $d->id]) ?>"
                               class="btn btn-default btn-sm br-round">Historique</a>

                            <a href="<?= $this->Url->build(['controller' => 'Contacts', 'action' => 'edit', $d->id]) ?>"
                               class="btn btn-default btn-sm br-round">EDITER</a>

                            <?= $this->Html->link(
                                'Supprimer',
                                [
                                    'controller' => 'Contacts', 'action' => 'delete', $d->id
                                ],
                                [
                                    'confirm' => 'êtes-vous sûr de vouloir supprimer le contact',
                                    'class' => "btn btn-danger btn-sm br-round"
                                ]
                            ) ?>

                        </td>

                    </tr>
                <?php endforeach; ?>

                </tbody>
            </table>


        </div>
    </div>
</div>


<div id="sms-template-container">
    <div id="sms-template-window">
        <h4>Choix du template SMS</h4>
        <hr/>
        <ul>
            <?php foreach ($templates as $template): ?>
                <li class="t-item" data-id="<?= $template->id ?>">
                    <div class="template-item">
                        <div>
                            <p>
                                <strong><?= $template->name ?></strong><br/>
                                <span>09/02/2021</span>
                            </p>
                            <span class="badge"><?= $template->active ? 'Actif' : 'Inactif' ?></span>
                        </div>
                    </div>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
</div>


<script type="text/javascript">

    $(document).ready(function () {

        const contactsTable = $('#contacts').DataTable({
            dom: 'Bfrtip',
            searching: true,

            columnDefs: [
                {
                    "targets": [0],
                    "visible": false,
                    "searchable": true
                },
                {
                    "targets": [1],
                    "visible": true,
                    "searchable": true,
                    "orderable": false
                }
            ],

            "pageLength": 100,

            order: [[0, "desc"]],

            language: {
                search: "",
                searchPlaceholder: "Recherche ...",
                processing: "Traitement en cours...",
                lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
                info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                infoPostFix: "",
                loadingRecords: "Chargement en cours...",
                zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
                emptyTable: "Aucune donnée disponible dans le tableau",
                paginate:
                    {
                        next: "Suivant",
                        previous: "Précédent",
                        entries: "Entrées",
                        showing: "Affichage",
                    }
            },

            "footerCallback": function (row, data, start, end, display) {
            }


        });
        const endpoint = "<?= $this->Url->build(['controller' => 'Contacts', 'action' => 'sendmessage']) ?>";

        const showMessageTemplateDialog = () => $('#sms-template-container').show();

        const hideMessageTemplateDialog = () => $('#sms-template-container').hide();

        const selectMessageTemplate = (id) => currentMessageTemplate = id;

        const clearSelectionList = () => {
            selectedContacts = [];
        };

        const toggleContactSelection = () => {
            // Toggle the selection status
            currentGroupSelection = !currentGroupSelection;
            $('.contact-item').prop('checked', currentGroupSelection);

            // Clear the contact selection
            clearSelectionList();

            // Update contact selection
            $('.contact-item').each(function (index) {
                if (currentGroupSelection) {
                    selectedContacts.push($(this).data('id'));
                }
            });
        };

        const unselectAllContacts = () => {
            $('.contact-item').prop('checked', false);
            $('.contact-select-all').prop('checked', false);
            clearSelectionList();
        }

        // Request the api
        const sendMessage = async (ids) => {

            if (currentMessageTemplate === '') {
                showMessageTemplateDialog();
                return false;
            }

            const request = await fetch(endpoint + '?ids=' + contactSelectionFormat(ids) + '&template=' + currentMessageTemplate)
            const result = await request.json();

            const responseMsg = result.results.success ? 'Messages envoyés' : 'Une erreur est survenue';
            alert(responseMsg);
            unselectAllContacts();
            currentMessageTemplate = '';
        }

        let selectedContacts = [];
        let currentGroupSelection = false;
        let currentMessageTemplate = '';

        const contactSelectionFormat = (params) => params.join(',');

        // Hide the default search input
        $('#contacts_filter').hide();

        // Replace the existing search field
        $('#contacts_search').keyup(function () {
            contactsTable.search($(this).val());
            contactsTable.draw();
        });

        // Single message sending btn
        $('.contact-send-msg').on('click', async function () {
            const ids = [];
            ids.push([].push($(this).data('id')));
            await sendMessage(ids);
        });

        // Click sur les lignes
        $('.contact-item').on('click', function () {

            const id = $(this).data('id');

            if (this.checked)
                selectedContacts.push(id);
            else
                selectedContacts = selectedContacts.filter(function (item) {
                    return item !== id;
                })

        });

        // Group selection
        $('.contact-select-all').on('click', () => toggleContactSelection());

        // Send message button action
        $('#send-group-btn').on('click', async function () {

            // Check if the selection is not empty
            if (selectedContacts.length === 0) {
                alert('Selectionnez au minimum un contact');
                return;
            }

            // Request the API
            await sendMessage(selectedContacts);

        });


        // SMS Template selection
        $('.t-item').on('click', async function () {
            selectMessageTemplate($(this).data('id'));
            hideMessageTemplateDialog();
        });

    });

</script>

<style type="text/css">

    input[type="search"] {
        padding: 8px;
    }

    table.dataTable thead th, table.dataTable thead td {
        padding: 10px 18px;
        border-bottom: none;
    }

    .filter-container {

        display: flex;

    }

    .filter-container > div {
        flex: 1;
        margin: 0;
    }

    .right-content {

        display: flex;
        justify-content: flex-end;

    }

    .right-content div {
        margin-left: 1em;

    }

    .flex-large {
        width: 400px;
    }

    .br-round {
        border-radius: 25px;
    }

    .badge {
        margin-left: 5px;
    }

    #sms-template-container {
        display: none;
        background: rgba(0, 0, 0, .5);
        position: fixed;
        z-index: 8000;
        left: 0;
        top: 0;
        bottom: 0;
        right: 0;
    }

    #sms-template-window {
        border-radius: 5px;
        background: white;
        width: 800px;
        height: 500px;
        overflow: scroll;
        overflow-x: hidden;
        padding: 1em;
        position: absolute;
        left: 50%;
        top: 50%;
        margin-left: -400px;
        margin-top: -250px;
    }

    #sms-template-window ul {
        padding: 0;
        margin: 0
    }

    #sms-template-window ul li {
        list-style: none;
        height: 50px;
        margin-bottom: 10px;
        padding: 1em;
        border-radius: 5px;
        cursor: pointer;
        display: block;
        height: auto;
        background: rgba(234, 232, 232, 0.2);
        border: solid 1px #3333;
    }

    #sms-template-window ul li:hover {
        background: #a4cd00;
        color: white;
    }

</style>
