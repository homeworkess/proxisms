<section id="wrapper" class="error-page">
    <div class="error-box">
        <div class="error-body text-center">
            <h1 class="text-danger">404</h1>
            <h3 class="text-uppercase">Page Intouvable !</h3>
            <p class="text-muted m-t-30 m-b-30">CLIQUEZ SUR LE BOUTON CI-DESSOUS POUR REVENIR</p>
            <a href="javascript:history.back()" class="btn btn-danger btn-rounded waves-effect waves-light m-b-40">Retourner en arrière</a> </div>
        <footer class="footer text-center">2017 © e-wadjba.</footer>
    </div>
</section>