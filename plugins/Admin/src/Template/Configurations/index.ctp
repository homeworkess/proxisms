<!-- ============================================================== -->
<!-- Preloader -->
<!-- ============================================================== -->
<div class="preloader">
    <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/>
    </svg>
</div>
<!-- ============================================================== -->
<!-- Wrapper -->
<!-- ============================================================== -->
<div id="wrapper">
    <!-- ============================================================== -->
    <!-- Topbar header - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <?= $this->Element('Admin.main_menu') ?>
    <!-- End Top Navigation -->
    <!-- ============================================================== -->
    <!-- Left Sidebar - style you can find in sidebar.scss  -->
    <!-- ============================================================== -->
    <?= $this->Element('Admin.left_menu') ?>
    <!-- ============================================================== -->
    <!-- End Left Sidebar -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Page Content -->
    <!-- ============================================================== -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">CONFIGURATION GLOBALE</h4></div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">

                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <!--row -->
            <?= $this->Form->create($config) ?>

            <div class="row">

                <div class="col-sm-8">
                    <ol class="breadcrumb" style="background-color: white">
                        <li><a href="#">ACCUEIL</a></li>
                        <li><a href="#">CONFIGURATION</a></li>
                        <li class="active">EDITER</li>
                    </ol>
                </div>

                <div class="col-lg-2">
                    <a href="<?= $this->Url->build(['controller' => 'Dashboard', 'action' => 'index']) ?>" type="submit"
                       class="btn btn-block btn-default pull-right">ANNULER</a>
                </div>

                <div class="col-lg-2">
                    <button type="submit" class="btn btn-block btn-wj pull-right">ENREGISTRER</button>
                </div>
            </div>


            <?= $this->Flash->render() ?>


            <div class="row">

                <div class="col-sm-6">

                    <div class="white-box">
                        <h3 class="box-title m-b-0">Gateway SMS ( Twilio )</h3>
                        <p class="text-muted m-b-30 font-13"> Les champs récédés d'un * sont obligatoires </p>
                        <div class="row">
                            <div class="col-sm-12 col-xs-12">


                                <div class="row">

                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <?= $this->Form->control('sid', ['class' => 'form-control', 'id' => 'sid','disabled'=>'true', 'placeholder' => 'SID', 'label' => '* SID']) ?>
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <?= $this->Form->control('token', ['class' => 'form-control', 'id' => 'token','disabled'=>'true', 'placeholder' => 'Token', 'label' => '* Token']) ?>
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <?= $this->Form->control('phone', ['class' => 'form-control', 'id' => 'phone','disabled'=>'true', 'placeholder' => 'Numéro mobile', 'label' => '* Numéro mobile']) ?>
                                        </div>
                                    </div>

                                </div>


                            </div>
                        </div>

                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="white-box">
                        <h3 class="box-title m-b-0">Template du SMS</h3>
                        <p class="text-muted m-b-30 font-13"></p>

                        <div class="form-group">
                            <?= $this->Form->textarea('body', ['class' => 'form-control', 'id' => 'body',
                                'placeholder' => 'Template SMS','maxlength' => '160', 'label' => '* Template SMS']) ?>
                        </div>


                        <p class="text-muted m-b-30 font-13">Tags dynamiques</p>
                        <ul>
                            <li>Nom : #[first_name]</li>
                            <li>Prénom : #[last_name]</li>
                            <li>Nom complet : #[full_name]</li>
                        </ul>

                    </div>
                </div>
            </div>
            <?= $this->Form->end() ?>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
        <?= $this->element('Admin.footer') ?>
    </div>
    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Wrapper -->
<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- All Jquery -->
<!-- ============================================================== -->

<!-- Bootstrap Core JavaScript -->
<?= $this->Html->script('/ample/bootstrap/dist/js/bootstrap.min.js') ?>

<!-- Menu Plugin JavaScript -->
<?= $this->Html->script('/ample/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js') ?>

<!--slimscroll JavaScript -->
<?= $this->Html->script('/ample/js/jquery.slimscroll.js') ?>

<!--Wave Effects -->
<?= $this->Html->script('/ample/js/waves.js') ?>

<!--Counter js -->
<?= $this->Html->script('/ample/plugins/bower_components/waypoints/lib/jquery.waypoints.js') ?>
<?= $this->Html->script('/ample/plugins/bower_components/counterup/jquery.counterup.min.js') ?>

<!-- Custom Theme JavaScript -->
<?= $this->Html->script('/ample/js/custom.js') ?>
<?= $this->Html->script('/js/vue.min') ?>
<?= $this->Html->script('/js/jquery.maskMoney.min') ?>
<?= $this->Html->script('/js/jquery.inputmask.bundle.min') ?>

<style type="text/css">

    .error-message {
        color: red;
        padding: 1em;
        padding-left: 0;
    }

    input.form-error {
        border: 1px solid red;

    }

</style>


<script type="text/javascript">


    /* AutoComplete de l'adresse et conversion en Longitude Lattitude */
    var autocomplete;
    var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
    };

    function initAutocomplete() {
        // Create the autocomplete object, restricting the search to geographical
        // location types.
        autocomplete = new google.maps.places.Autocomplete(
            (document.getElementById('address')),
            {
                types: ['geocode'],
                componentRestrictions: {country: "dz"}
            },
        );

        // When the user selects an address from the dropdown, populate the address
        // fields in the form.
        autocomplete.addListener('place_changed', fillInAddress);
    }

    function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace().geometry.location;

        $('#latitude').val(place.lat());
        $('#longitude').val(place.lng());

    }

    /* Masque de saisie Money */
    $('#credit').maskMoney({
        thousands: ' ',
        allowZero: false,
        allowNegative: false,
        precision: 0
    });

    $('#notifications_threshold').maskMoney({
        thousands: ' ',
        allowZero: false,
        allowNegative: false,
        precision: 0
    });

    $('#rc_n').inputmask("99-a-9999999");
    $('#iden_fiscal').inputmask("999999999999999");


</script>

<script
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBGt1Xc6EIEyiddx_-fQp6grRqBQIbpa7w&libraries=places&callback=initAutocomplete"></script>
