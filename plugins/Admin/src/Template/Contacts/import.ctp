<?= $this->Element('Admin.loading') ?>
<!-- ============================================================== -->
<!-- Wrapper -->
<!-- ============================================================== -->
<div id="wrapper">
    <!-- ============================================================== -->
    <!-- Topbar header - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <?= $this->Element('Admin.main_menu') ?>
    <!-- End Top Navigation -->
    <!-- ============================================================== -->
    <!-- Left Sidebar - style you can find in sidebar.scss  -->
    <!-- ============================================================== -->
    <?= $this->Element('Admin.left_menu') ?>
    <!-- ============================================================== -->
    <!-- End Left Sidebar -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Page Content -->
    <!-- ============================================================== -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">Importer des contacts</h4></div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">

                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <!--row -->
            <?= $this->Form->create($contact,['enctype' => 'multipart/form-data']) ?>

            <div class="row">

                <div class="col-sm-12">
                    <ol class="breadcrumb" style="background-color: white">
                        <li><a href="#">ACCUEIL</a></li>
                        <li><a href="#">CONTACTS</a></li>
                        <li class="active">IMPORTER</li>
                    </ol>
                </div>

            </div>

            <?= $this->Flash->render() ?>

            <div class="row">

                <div class="col-sm-6">

                    <div class="white-box">
                        <h3 class="box-title m-b-0">Selectionnez un ficher à importer</h3>
                        <p class="text-muted m-b-30 font-13"> Seul le format .CSV est accepté </p>
                        <div class="row">
                            <div class="col-sm-12 col-xs-12">

                                <div class="form-group">
                                    <?= $this->Form->input('contacts_file', ['type' => 'file',
                                        'class' => 'form-control',
                                        'id' => 'contacts_file', 'placeholder' => 'Importer', 'label' => '* Selectionnez un ficher à importer']) ?>
                                </div>

                                <div class="col-lg-4">
                                    <button type="submit" class="btn btn-block btn-wj pull-right">IMPORTER</button>
                                </div>

                            </div>
                        </div>

                    </div>


                </div>

            </div>
            <?= $this->Form->end() ?>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
        <?= $this->element('Admin.footer') ?>
    </div>
    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Wrapper -->
<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- All Jquery -->
<!-- ============================================================== -->

<!-- Bootstrap Core JavaScript -->
<?= $this->Html->script('/ample/bootstrap/dist/js/bootstrap.min.js') ?>

<!-- Menu Plugin JavaScript -->
<?= $this->Html->script('/ample/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js') ?>

<!--slimscroll JavaScript -->
<?= $this->Html->script('/ample/js/jquery.slimscroll.js') ?>

<!--Wave Effects -->
<?= $this->Html->script('/ample/js/waves.js') ?>

<!--Counter js -->
<?= $this->Html->script('/ample/plugins/bower_components/waypoints/lib/jquery.waypoints.js') ?>
<?= $this->Html->script('/ample/plugins/bower_components/counterup/jquery.counterup.min.js') ?>

<!-- Custom Theme JavaScript -->
<?= $this->Html->script('/ample/js/custom.js') ?>
<?= $this->Html->script('/js/vue.min') ?>
<?= $this->Html->script('/js/jquery.maskMoney.min') ?>


