<?= $this->Element('Admin.loading') ?>
<!-- ============================================================== -->
<!-- Wrapper -->
<!-- ============================================================== -->
<div id="wrapper">
    <!-- ============================================================== -->
    <!-- Topbar header - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <?= $this->Element('Admin.main_menu') ?>
    <!-- End Top Navigation -->
    <!-- ============================================================== -->
    <!-- Left Sidebar - style you can find in sidebar.scss  -->
    <!-- ============================================================== -->
    <?= $this->Element('Admin.left_menu') ?>
    <!-- ============================================================== -->
    <!-- End Left Sidebar -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Page Content -->
    <!-- ============================================================== -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">Fiche contact</h4></div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">

                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <!--row -->
            <?= $this->Form->create($contact) ?>

            <div class="row">
                <div class="col-sm-8">
                    <ol class="breadcrumb" style="background-color: white">
                        <li><a href="#">ACCUEIL</a></li>
                        <li><a href="#">CONTACT</a></li>
                        <li class="active">NOUVEAU</li>
                    </ol>
                </div>

                <div class="col-lg-2">
                    <a href="<?= $this->Url->build(['action' => 'index']) ?>" type="submit"
                       class="btn btn-block btn-default pull-right">ANNULER</a>
                </div>

                <div class="col-lg-2">
                    <button type="submit" class="btn btn-block btn-wj pull-right">ENREGISTRER</button>
                </div>

            </div>

            <?= $this->Flash->render() ?>

            <div class="row">

                <div class="col-sm-6">

                    <div class="white-box">
                        <h3 class="box-title m-b-0">Informations sur le contact</h3>
                        <p class="text-muted m-b-30 font-13"> Les champs récédés d'un * sont obligatoires </p>
                        <div class="row">
                            <div class="col-sm-12 col-xs-12">

                                <div class="row">

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <?= $this->Form->control('first_name', ['class' => 'form-control', 'id' => 'first_name', 'placeholder' => 'Nom', 'label' => '* Nom']) ?>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <?= $this->Form->control('last_name', ['class' => 'form-control', 'id' => 'last_name', 'placeholder' => 'Prénom', 'label' => '* Prénom']) ?>
                                        </div>
                                    </div>

                                </div>

                                <div class="row">

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <?= $this->Form->input('mobile_phone', ['class' => 'form-control', 'id' => 'mobile_phone', 'placeholder' => 'MOBILE', 'label' => 'MOBILE']) ?>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <?= $this->Form->input('tags', ['class' => 'form-control', 'id' => 'tags', 'placeholder' => 'TAGS', 'label' => 'TAGS ( Séparateur , )']) ?>
                                        </div>
                                    </div>

                                </div>

                                <div class="form-group">
                                    <?= $this->Form->input('address', ['type' => 'textarea', 'class' => 'form-control', 'id' => 'address', 'placeholder' => 'ADRESSE', 'label' => '* ADRESSE']) ?>
                                </div>

                            </div>
                        </div>

                    </div>


                </div>

            </div>
            <?= $this->Form->end() ?>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
        <?= $this->element('Admin.footer') ?>
    </div>
    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Wrapper -->
<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- All Jquery -->
<!-- ============================================================== -->

<!-- Bootstrap Core JavaScript -->
<?= $this->Html->script('/ample/bootstrap/dist/js/bootstrap.min.js') ?>

<!-- Menu Plugin JavaScript -->
<?= $this->Html->script('/ample/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js') ?>

<!--slimscroll JavaScript -->
<?= $this->Html->script('/ample/js/jquery.slimscroll.js') ?>

<!--Wave Effects -->
<?= $this->Html->script('/ample/js/waves.js') ?>

<!--Counter js -->
<?= $this->Html->script('/ample/plugins/bower_components/waypoints/lib/jquery.waypoints.js') ?>
<?= $this->Html->script('/ample/plugins/bower_components/counterup/jquery.counterup.min.js') ?>

<!-- Custom Theme JavaScript -->
<?= $this->Html->script('/ample/js/custom.js') ?>
<?= $this->Html->script('/js/vue.min') ?>
<?= $this->Html->script('/js/jquery.maskMoney.min') ?>

<?= $this->Html->script('/js/jquery.inputmask.bundle.min') ?>

<style type="text/css">

    .error-message {
        color: red;
        padding: 1em;
        padding-left: 0;
    }

    input.form-error {
        border: 1px solid red;

    }

</style>


<script type="text/javascript">

    // $(document).ready(function(){
    //     $('#mobile_phone').inputmask("+(999)-99999999999");
    //     //$('#mobile_phone').inputmask("+999-a-9999999");
    // });


</script>
