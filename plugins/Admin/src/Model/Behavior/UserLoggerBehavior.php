<?php
namespace Admin\Model\Behavior;

use Cake\ORM\Behavior;
use Cake\ORM\Table;
use ArrayObject;
use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\Network\Session;

class UserLoggerBehavior extends Behavior
{
    protected $_defaultConfig = [];

    public function beforeSave(Event $event, EntityInterface $entity, ArrayObject $options)
    {
        $entity->uid = (new Session())->read('Auth.Admin')['id'];
        return true;
    }

}
