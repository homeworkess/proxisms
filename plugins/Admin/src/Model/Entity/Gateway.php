<?php
namespace Admin\Model\Entity;

use Cake\ORM\Entity;

/**
 * Gateway Entity
 *
 * @property int $id
 * @property string $name
 * @property string $code
 * @property array|null $config
 * @property bool|null $active
 * @property bool $selected
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \Admin\Model\Entity\Schedule[] $schedules
 */
class Gateway extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'code' => true,
        'config' => true,
        'active' => true,
        'selected' => true,
        'created' => true,
        'modified' => true,
        'schedules' => true,
    ];
}
