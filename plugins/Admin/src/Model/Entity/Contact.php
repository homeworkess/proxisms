<?php
namespace Admin\Model\Entity;

use Cake\ORM\Entity;

/**
 * Contact Entity
 *
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $mobile_phone
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 */
class Contact extends Entity
{
    const mobileClearPatterns = ['(',')','-','_'];

    protected $_accessible = [
        '*' => true,
    ];

//    protected function _setMobile_phone($mobile)
//    {
//        return array_reduce(self::mobileClearPatterns, function($carry, $pattern) {
//            return trim( str_replace($pattern,'',$carry) );
//        },$mobile);
//    }

}
