<?php

namespace Admin\Model\Table;

use Cake\Database\Exception;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Contacts Model
 *
 * @method \Admin\Model\Entity\Contact get($primaryKey, $options = [])
 * @method \Admin\Model\Entity\Contact newEntity($data = null, array $options = [])
 * @method \Admin\Model\Entity\Contact[] newEntities(array $data, array $options = [])
 * @method \Admin\Model\Entity\Contact|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Admin\Model\Entity\Contact saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Admin\Model\Entity\Contact patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Admin\Model\Entity\Contact[] patchEntities($entities, array $data, array $options = [])
 * @method \Admin\Model\Entity\Contact findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ContactsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->setTable('contacts');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
        $this->addBehavior('Timestamp');
        $this->hasMany('Schedules');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('first_name')
            ->maxLength('first_name', 50)
            ->requirePresence('first_name', 'create')
            ->notEmptyString('first_name');

        $validator
            ->scalar('last_name')
            ->maxLength('last_name', 50)
            ->requirePresence('last_name', 'create')
            ->notEmptyString('last_name');

        $validator
            ->scalar('mobile_phone')
            ->regex('mobile_phone','/^((\+)33|0)[1-9](\d{2}){4}$/','Veuillez saisir un numéro de mobile valide (+33...)')
            ->requirePresence('mobile_phone', 'create')
            ->notEmptyString('mobile_phone');

        return $validator;
    }

    public function buildRules(RulesChecker $rules): RulesChecker
    {
        //$rules->add($rules->isUnique(['mobile_phone'], 'Le numéro doit être unique'));
        return $rules;
    }

    public function import(array $data): bool
    {

        $entities = [];

        foreach ($data as $item){

            $mobilePhone = array_reduce(['(',')','-','_',' '], function ($carry, $pattern) {
                return trim(str_replace($pattern, '', $carry));
            }, $item['mobile_phone']);

            $entities[] = $this->newEntity([
                'first_name' => $item['first_name'],
                'last_name' => $item['last_name'],
                'mobile_phone' => strpos($mobilePhone, "+33") ?:  '+33' . (int)$mobilePhone,
                'tags' => $item['tags'],
                'address' => $item['address'],
            ]);

        }

        // Persist
        try {

            foreach ($entities as $entity){
                $this->save($entity);
            }

            return true;

        } catch (Exception $e) {

            return false;

        }

    }

}
