<?php

namespace Admin\Controller;

use Admin\Controller\AppController;
use Admin\Form\AccrediterForm;
use Admin\Form\ContactsImportForm;
use Admin\Repositories\ShortMessageRepository;
use Admin\Services\ConfigService;
use Admin\Services\ShortMessageService;
use Admin\UseCase\SendMessage;
use Cake\ORM\TableRegistry;
use mysql_xdevapi\Exception;

class ContactsController extends AppController
{

    public function beforeRender(\Cake\Event\Event $event)
    {
        $this->viewBuilder()->setLayout('Admin.company_edit');
    }

    public function index()
    {
        $contacts = $this->Contacts->find('all')->where(['deleted' => 0]);
        $templates = TableRegistry::getTableLocator()
            ->get('Templates')
            ->find()
            ->where(['deleted' => 0])
            ->orderAsc('sequence');

        $this->set(compact('contacts', 'templates'));
        $this->render();
    }

    public function view($id = null)
    {
        $company = $this->Companies->get($id, [
            'contain' => ['Users', 'Accounts', 'CompanyTypes']
        ]);
        $this->set('company', $company);
    }

    public function add()
    {

        $this->loadComponent('Admin.AccountPasswordHelper');

        $contact = $this->Contacts->newEntity();

        if ($this->request->is('post')) {

            $contact = $this->Contacts->patchEntity($contact, $this->request->getData());

            if ($this->Contacts->save($contact)) {
                $this->Flash->success(strtoupper(__('Le client a été correctement ajouté')));
                return $this->redirect(['action' => 'index']);
            }

            $this->Flash->error(strtoupper(__('Une erreur est survenue lors de lajout du client')));
        }

        $this->set(compact('contact'));
    }

    public function edit($id = null)
    {
        $contact = $this->Contacts->get($id);

        if ($this->request->is(['patch', 'post', 'put'])) {

            $contact = $this->Contacts->patchEntity($contact, $this->request->getData());

            if ($this->Contacts->save($contact)) {

                $this->Flash->success(__('Les nouvelles informations ont été enregistrées'));
                return $this->redirect(['action' => 'index']);

            }

            $this->Flash->error(__(strtoupper("Une erreur est servenue l'ors de la modification")));

        }
        $this->set(compact('contact'));
    }

    public function sendMessage()
    {

        $data = $this->request->getQuery('ids');
        $templateId = $this->request->getQuery('template');
        $loggedUserId = $this->getRequest()->getSession()->read('Auth.Admin.id');
        $messageService = new ShortMessageService($loggedUserId);

        try {

            $result = $messageService->sendBatchMessages(
                explode(',', $data),
                $templateId
            );

            return $this->response
                ->withStatus(200)
                ->withHeader('Content-Type', 'application/json')
                ->withStringBody(
                    json_encode([
                        'results' => [
                            'success' => true,
                            'data' => $result
                        ]
                    ])
                );


        } catch (\Exception $e) {

            return $this->response
                ->withStatus(400)
                ->withHeader('Content-Type', 'application/json')
                ->withStringBody(
                    json_encode([
                        'results' => [
                            'success' => false,
                            'error_message' => $e->getMessage()
                        ]
                    ])
                );

        }

    }

    public function history($id)
    {
        $messages = TableRegistry::getTableLocator()
            ->get('Admin.Schedules')
            ->find('all')
            ->where(['contact_id' => $id])
            ->contain(['Contacts']);

        $this->set('messages', $messages);
    }

    public function import()
    {

        $contactsImportForm = new ContactsImportForm();

        if ($this->request->is('post')) {

            if ($contactsImportForm->execute($this->request->getData())) {

                $model = $this->Contacts;

                if ($model->import($contactsImportForm->getData())) {

                    $this->Flash->success('Contacts importés avec succès');

                } else {

                    $this->Flash->error('Il y a eu un problème lors de l\'importation de la liste de contacts');

                }


            } else {

                $this->Flash->error('Il y a eu un problème lors de l\'importation de la liste de contacts');

            }

            $this->redirect(['action' => 'index']);

        }

        $this->set('contact', $contactsImportForm);

    }

    public function delete($id)
    {
        $this->Contacts->updateAll(['deleted' => 1], ['id' => $id]);
        $this->Flash->success('Contact supprimé');
        $this->redirect(['action' => 'index']);
    }


}
