<?php

namespace Admin\Controller;

use Admin\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Configurations Controller
 *
 *
 * @method \Admin\Model\Entity\Configuration[] paginate($object = null, array $settings = [])
 */
class ConfigurationsController extends AppController
{

    public function beforeRender(\Cake\Event\Event $event)
    {
        $this->viewBuilder()->setLayout('Admin.company_edit');
    }


    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Admin.Config');
        $this->loadModel('Admin.Users');
        $this->loadModel('Admin.Contacts');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        //$config = $this->Config->fetchConfigurationByName('twilio');
        //$defaultTemplate = $this->Config->fetchDefaultTemplate();

        $model = TableRegistry::getTableLocator()->get('Templates');
        $config = $model->get(1);

        if ($this->request->is(['post', 'put'])) {
            $config = $model->patchEntity($config, $this->request->getData());
            if ($model->save($config)) {
                $this->Flash->success(__('Configuration mise à jour'));
                return $this->redirect([
                    'action'=>'index'
                ]);
            }
            $this->Flash->error(__('Une erreur est survenue lors de la mise à jour de la configuration'));
        }

        $this->set(compact('config'));
    }


}
