<?php


namespace Admin\Controller;

use Admin\Controller\AppController;

class TemplatesController extends AppController
{

    public function initialize()
    {
        parent::initialize();
    }

    public function index()
    {
        $templates = $this->Templates->find('all')
            ->where(['deleted' => 0])
            ->orderAsc('sequence')
            ->toArray();
        $this->set('templates',$templates);
    }

    public function add()
    {
        $template = $this->Templates->newEntity();

        if($this->getRequest()->isPost()){

            $this->Templates->patchEntity($template, $this->getRequest()->getData());

            if ($this->Templates->save($template)) {
                $this->Flash->success(strtoupper(__('Le template a été correctement ajouté')));
                return $this->redirect(['action' => 'index']);
            }

            $this->Flash->error(strtoupper(__('Une erreur est survenue lors de la création du template')));

        }


        $this->set('template',$template);
    }

    public function edit($id)
    {
        $template = $this->Templates->get($id);

        if($this->getRequest()->is(['post','put','patch'])){

            $this->Templates->patchEntity($template, $this->getRequest()->getData());

            if ($this->Templates->save($template)) {
                $this->Flash->success(strtoupper(__('Le template a été correctement ajouté')));
                return $this->redirect(['action' => 'index']);
            }

            $this->Flash->error(strtoupper(__('Une erreur est survenue lors de la création du template')));

        }

        $this->set('template',$template);
    }

    public function delete($id)
    {
        $this->Templates->updateAll(['deleted' => 1], ['id' => $id]);
        $this->Flash->success('Template supprimé');
        $this->redirect(['action' => 'index']);

    }

}
