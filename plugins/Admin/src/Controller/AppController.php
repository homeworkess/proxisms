<?php

namespace Admin\Controller;

use App\Controller\AppController as BaseController;

class AppController extends BaseController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Csrf');
        $this->loadComponent('Flash');
    }

    public function beforeRender(\Cake\Event\Event $event)
    {
        parent::beforeRender($event);
        $this->viewBuilder()->setLayout('Admin.dashboard');
    }

    public function beforeFilter(\Cake\Event\Event $event)
    {
        parent::beforeFilter($event);
        if ($this->request->getSession()->check('Auth.Admin'))
            return true;

        return $this->redirect([
            'plugin' => 'Authentication',
            'controller' => 'Users',
            'action' => 'login'
        ]);
    }

}
