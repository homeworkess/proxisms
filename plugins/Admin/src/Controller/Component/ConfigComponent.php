<?php
namespace Admin\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\ORM\TableRegistry;

/**
 * Configuration component
 */
class ConfigComponent extends Component
{
    private $repository;
    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];

    public function initialize(array $config)
    {
        parent::initialize($config);
    }

    public function fetchConfigurationByName($name){
        return TableRegistry::getTableLocator()->get('Configurations')->find('all',[
            'conditions' => ['name' => $name]
        ])->first();
    }

    public function fetchDefaultTemplate()
    {
        return TableRegistry::getTableLocator()->get('templates')->find('all',[
            'conditions' => ['active' => 1]
        ])->first();
    }

    public function fetchDefault(){

        $defaultGatewayConfig = $this->fetchConfigurationByName('twilio');
        $defaultMessageTemplate = $this->fetchDefaultTemplate();
        return [
          'sid' =>  $defaultGatewayConfig->sid,
          'token' =>  $defaultGatewayConfig->token,
          'token' =>  $defaultGatewayConfig->token,
        ];
    }

}
