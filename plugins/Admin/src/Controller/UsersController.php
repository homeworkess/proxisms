<?php


namespace Admin\Controller;

use Admin\Controller\AppController;

class UsersController extends AppController
{

    public function initialize()
    {
        parent::initialize();
    }

    public function index()
    {
        $users = $this->Users->find('all')->contain('Groups')->toArray();
        $this->set('users',$users);
    }

    public function add()
    {
        $user = $this->Users->newEntity();
        $groups = $this->Users->Groups->find('list');

        if($this->getRequest()->isPost()){

            $this->Users->patchEntity($user, $this->getRequest()->getData());

            if ($this->Users->save($user)) {

                $this->Flash->success(strtoupper(__('L\'utilisateur a été correctement ajouté')));
                return $this->redirect(['action' => 'index']);

            }

            $this->Flash->error(strtoupper(__('Une erreur est survenue lors de l\'ajout de l\'utilisateur')));

        }


        $this->set(compact('user','groups'));
    }

    public function edit($id)
    {
        $user = $this->Users->get($id);
        $groups = $this->Users->Groups->find('list');

        if($this->getRequest()->is(['post','put','patch'])){

            $this->Users->patchEntity($user, $this->getRequest()->getData());

            if ($this->Users->save($user)) {
                $this->Flash->success(strtoupper(__('Operation effectué')));
                return $this->redirect(['action' => 'index']);
            }

            $this->Flash->error(strtoupper(__('Une erreur est survenue lors de l\'edition du compte')));

        }

        $this->set(compact('user','groups'));
    }

}
