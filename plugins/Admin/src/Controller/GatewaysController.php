<?php


namespace Admin\Controller;


use Admin\Form\GatewayFormFactory;

class GatewaysController extends AppController
{
    public function initialize()
    {
        parent::initialize();
    }

    public function index()
    {
        $gateways = $this->Gateways->find('all')->toArray();
        $this->set('gateways', $gateways);
    }

    public function add()
    {
        //$gateway = $this->Gateways->newEntity();
        //$this->set('gateway',$gateway);
    }

    public function edit($id)
    {
        $gateway = $this->Gateways->get($id);
        $gatewayForm = GatewayFormFactory::makeForm($gateway->code);
        $gatewayView = GatewayFormFactory::makeView($gateway->code);

        if ($this->getRequest()->is(['post', 'put', 'patch'])) {

            if ($gatewayForm->validate($this->getRequest()->getData())) {

                $this->Gateways->patchEntity($gateway, $gatewayForm->buildEntityFormat(
                    $this->getRequest()->getData()
                ));

                if ($this->Gateways->save($gateway)) {

                    $this->Flash->success('Configuration sauvegardée');
                    $this->redirect(['action' => 'index']);

                } else {

                    $this->Flash->error('Vérifiez correctement la configuration avant de sauvegarder');

                }

            } else {

                $this->Flash->error('Vérifiez correctement la configuration avant de sauvegarder');

            }

        }

        $gatewayForm->setData($gateway->toArray());

        $this->set('gateway', $gatewayForm);
        $this->render($gatewayView);

    }

}
