<?php

namespace Admin\Controller;

use Admin\Controller\AppController;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;

class DashboardController extends AppController
{

    public function beforeRender(\Cake\Event\Event $event)
    {
        $this->viewBuilder()->setLayout('Admin.dashboard');
    }

    public function index(){

        $total = TableRegistry::getTableLocator()
            ->get('Admin.Schedules')
            ->find()
            ->count();

        $sent = TableRegistry::getTableLocator()
            ->get('Admin.Schedules')
            ->find()
            ->where(['status' => 'sent'])
            ->count();

        $pending = TableRegistry::getTableLocator()
            ->get('Admin.Schedules')
            ->find()
            ->where(['status' => 'pending'])
            ->count();

        // Current Month Calendar
        $daysCount = cal_days_in_month(CAL_GREGORIAN,Date('m'),Date('Y'));

        $dayslist     = [];
        $dayslistData = [];

        /* Création des journée d'un calendrier */
        for($i=1; $i<=$daysCount; $i++)
        {
            $dayslist[] = $i;
            $dayslistData[(int)$i] = 0;
        }

        /* TEST SELECTION RAPIDE */
        $month = Date('m');
        $year = Date('Y');

        $conn = ConnectionManager::get('default');
        $query = "SELECT COUNT(*) as sent_sms,DAY(created) as day_index FROM schedules WHERE MONTH(created) = '$month' AND YEAR(created) = '$year' GROUP BY created";

        $stmt = $conn->execute($query);
        $rows = $stmt->fetchAll('assoc');

        /* A OPTIMISER UTILISER UN CACHE */
        foreach ($rows as $row){
            $dayslistData[$row['day_index']] = (int)$row['sent_sms'];
        }

        $this->set(
            compact('total','sent','pending','dayslist','dayslistData')
        );

    }

}
