<?php

namespace Admin\Controller;

use Admin\Controller\AppController;
use Admin\Form\AccrediterForm;
use Admin\Repositories\ShortMessageRepository;
use Admin\Services\ConfigService;
use Admin\Services\ShortMessageService;
use Admin\UseCase\SendMessage;
use Cake\ORM\TableRegistry;
use mysql_xdevapi\Exception;

class HistoryController extends AppController
{

    public function beforeRender(\Cake\Event\Event $event)
    {
        $this->viewBuilder()->setLayout('Admin.company_edit');
    }

    public function index()
    {
        $messages = TableRegistry::getTableLocator()
            ->get('Admin.Schedules')
            ->find()
            ->contain(['Contacts', 'Users', 'Gateways'])
            ->where(['Schedules.status IS NOT' => 'deleted'])
            ->orderDesc('Schedules.id');

        $this->set('messages',$messages);
    }

    public function delete($id)
    {
        $model = TableRegistry::getTableLocator()
            ->get('Admin.Schedules');


        $model->updateAll(['status' => 'deleted'], ['id' => $id]);
        $this->Flash->success('SMS Supprimé');
        $this->redirect(['action' => 'index']);

    }

}
