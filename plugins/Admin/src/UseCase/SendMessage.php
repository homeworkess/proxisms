<?php

namespace Admin\UseCase;

class SendMessage
{
    private $shortMessageRepository;
    private $contactId;
    private $messageBody;
    private $userId;

    public function __construct(
        $shortMessageRepository,
        $contactId,
        $messageBody,
        $userId
    )
    {
        $this->shortMessageRepository = $shortMessageRepository;
        $this->messageBody = $messageBody;
        $this->contactId = $contactId;
        $this->userId = $userId;
    }

    public function execute(){
        return $this->shortMessageRepository->createMessage(
            $this->contactId,
            $this->messageBody,
            $this->userId
        );
    }

}
