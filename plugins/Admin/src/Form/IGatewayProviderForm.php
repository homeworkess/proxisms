<?php


namespace Admin\Form;

interface IGatewayProviderForm
{
    function buildEntityFormat(array $data): array;
}
