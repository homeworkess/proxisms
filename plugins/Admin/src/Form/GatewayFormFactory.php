<?php
namespace Admin\Form;

use Cake\Form\Form;

class GatewayFormFactory
{

    public static function makeForm(string $code): Form
    {
        switch ($code){
            case "TWL":
                return new TwilioConfigForm();
                break;
            case "OVH":
                return new OvhConfigForm();
                break;
            default:
                throw new \Exception("No provider for this gateway");
                break;
        }
    }

    public static function makeView(string $code): string
    {
        switch ($code){
            case "TWL":
                return 'twilio_edit';
                break;
            case "OVH":
                return 'ovh_edit';
                break;
            default:
                throw new \Exception("No Form available for this gateway");
                break;
        }
    }


}
