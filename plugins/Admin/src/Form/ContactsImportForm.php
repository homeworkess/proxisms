<?php
namespace Admin\Form;

use Cake\Form\Form;
use Cake\Form\Schema;
use Cake\Validation\Validator;
use League\Csv\Reader;
use League\Csv\Statement;

/**
 * ContactsImport Form.
 */
class ContactsImportForm extends Form
{
    /**
     * Builds the schema for the modelless form
     *
     * @param \Cake\Form\Schema $schema From schema
     * @return \Cake\Form\Schema
     */
    protected function _buildSchema(Schema $schema)
    {
        return $schema;
    }

    /**
     * Form validation builder
     *
     * @param \Cake\Validation\Validator $validator to use against the form
     * @return \Cake\Validation\Validator
     */
    protected function _buildValidator(Validator $validator)
    {
        return $validator;
    }

    /**
     * Defines what to execute once the Form is processed
     *
     * @param array $data Form data.
     * @return bool
     */
    protected function _execute(array $data): bool
    {
        $allowedExtensions = ['csv'];

        $originTemporaryName = $data['contacts_file']['name'];
        $fileTemporaryName = $data['contacts_file']['tmp_name'];
        $fileType = $data['contacts_file']['type'];
        $fileExtension = substr(strtolower(strrchr($originTemporaryName, '.')), 1);

        // validate extension
        if(in_array($fileExtension, $allowedExtensions))
        {
            // Process the entries
            $reader = Reader::createFromPath($fileTemporaryName,'r');
            $reader->setHeaderOffset(0); // ignore first line for labels
            $reader->setDelimiter(';');

            $stmt = Statement::create();
            $entries = $stmt->process($reader);
            $records = [];

            foreach ($entries as $entry){

                $recordValues = array_values($entry);

                array_push($records,[
                    'first_name' => utf8_encode($recordValues[0]),
                    'last_name' => utf8_encode($recordValues[1]),
                    'mobile_phone' => utf8_encode($recordValues[2]),
                    'tags' => utf8_encode(isset($recordValues[3]) ?: ''),
                    'address' => utf8_encode(isset($recordValues[4]) ?: ''),
                ]);

            }

            $this->setData($records);

            return true;
        }

        return false;
    }
}
