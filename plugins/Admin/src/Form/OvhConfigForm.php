<?php
namespace Admin\Form;

use Cake\Form\Form;
use Cake\Form\Schema;
use Cake\Validation\Validator;

/**
 * OvhConfig Form.
 */
class OvhConfigForm extends Form implements IGatewayProviderForm
{
    /**
     * Builds the schema for the modelless form
     *
     * @param \Cake\Form\Schema $schema From schema
     * @return \Cake\Form\Schema
     */
    protected function _buildSchema(Schema $schema)
    {
        return $schema;
    }

    /**
     * Form validation builder
     *
     * @param \Cake\Validation\Validator $validator to use against the form
     * @return \Cake\Validation\Validator
     */
    protected function _buildValidator(Validator $validator): Validator
    {
        $validator
            ->scalar('name')
            ->requirePresence('name')
            ->notEmptyString('name');

        $validator
            ->scalar('code')
            ->inList('code',['OVH','TWL'])
            ->requirePresence('code')
            ->notEmptyString('code');

        $validator
            ->scalar('endPoint')
            ->requirePresence('endPoint')
            ->notEmptyString('endPoint');

        $validator
            ->scalar('applicationKey')
            ->requirePresence('applicationKey')
            ->notEmptyString('applicationKey');

        $validator
            ->scalar('applicationSecret')
            ->requirePresence('applicationSecret')
            ->notEmptyString('applicationSecret');

        $validator
            ->scalar('consumerKey')
            ->requirePresence('consumerKey')
            ->notEmptyString('consumerKey');

        $validator
            ->integer('active')
            ->requirePresence('active');

        $validator
            ->integer('selected')
            ->requirePresence('selected');

        return $validator;
    }

    /**
     * Defines what to execute once the Form is processed
     *
     * @param array $data Form data.
     * @return bool
     */
    protected function _execute(array $data)
    {
        return true;
    }


    /**
     * @param array $data
     */
    public function setData(array $data): void
    {
        $config = json_decode($data['config'],true) ?: [];

        $fields = [
            'name' => $data['name'],
            'code' => $data['code'],
            'selected' => $data['selected'],
            'active' => $data['active'],
        ];

        parent::setData( array_merge($fields, $config) );
    }


    public function buildEntityFormat(array $data): array
    {
        $config = [
            'endPoint' => $data['endPoint'],
            'applicationKey' => $data['applicationKey'],
            'applicationSecret' => $data['applicationSecret'],
            'consumerKey' => $data['consumerKey']
        ];

        return [
            'name' => $data['name'],
            'code' => $data['code'],
            'active' => $data['active'],
            'selected' => $data['selected'],
            'config' => json_encode($config)
        ];
    }

}
