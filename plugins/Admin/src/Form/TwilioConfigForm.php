<?php

namespace Admin\Form;

use Cake\Form\Form;
use Cake\Form\Schema;
use Cake\Validation\Validator;

/**
 * TwilioConfig Form.
 */
class TwilioConfigForm extends Form implements IGatewayProviderForm
{
    /**
     * Builds the schema for the modelless form
     *
     * @param \Cake\Form\Schema $schema From schema
     * @return \Cake\Form\Schema
     */
    protected function _buildSchema(Schema $schema)
    {
        return $schema;
    }

    /**
     * Form validation builder
     *
     * @param \Cake\Validation\Validator $validator to use against the form
     * @return \Cake\Validation\Validator
     */
    protected function _buildValidator(Validator $validator)
    {

        $validator
            ->scalar('sid')
            ->maxLength('sms_body', 160)
            ->requirePresence('sid')
            ->notEmptyString('sid');

        $validator
            ->scalar('token')
            ->maxLength('token', 160)
            ->requirePresence('token')
            ->notEmptyString('token');

        $validator
            ->scalar('from')
            ->maxLength('from', 160)
            ->requirePresence('from')
            ->notEmptyString('from');

        $validator
            ->integer('active')
            ->requirePresence('active');

        $validator
            ->integer('selected')
            ->requirePresence('selected');

        return $validator;
    }

    /**
     * Defines what to execute once the Form is processed
     *
     * @param array $data Form data.
     * @return bool
     */
    protected function _execute(array $data)
    {
        return true;
    }

    /**
     * @param array $data
     */
    public function setData(array $data): void
    {
        $config = json_decode($data['config'],true)?: [];

        $fields = [
            'name' => $data['name'],
            'code' => $data['code'],
            'selected' => $data['selected'],
            'active' => $data['active'],
        ];

        parent::setData( array_merge($fields, $config) );
    }

    function buildEntityFormat(array $data): array
    {
        $config = [
            'sid' => $data['sid'],
            'token' => $data['token'],
            'from' => $data['from'],
        ];

        return [
            'name' => $data['name'],
            'code' => $data['code'],
            'config' => json_encode($config),
            'selected' => $data['selected'],
            'active' => $data['active'],
        ];
    }
}
