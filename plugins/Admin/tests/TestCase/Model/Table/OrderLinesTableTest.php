<?php
namespace Admin\Test\TestCase\Model\Table;

use Admin\Model\Table\OrderLinesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * Admin\Model\Table\OrderLinesTable Test Case
 */
class OrderLinesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \Admin\Model\Table\OrderLinesTable
     */
    public $OrderLines;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.admin.order_lines',
        'plugin.admin.products',
        'plugin.admin.orders',
        'plugin.admin.companies',
        'plugin.admin.users',
        'plugin.admin.accounts',
        'plugin.admin.cards',
        'plugin.admin.card_types',
        'plugin.admin.card_operations',
        'plugin.admin.employees'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('OrderLines') ? [] : ['className' => OrderLinesTable::class];
        $this->OrderLines = TableRegistry::get('OrderLines', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->OrderLines);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
