<?php
namespace Admin\Test\TestCase\Model\Table;

use Admin\Model\Table\GatewaysTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * Admin\Model\Table\GatewaysTable Test Case
 */
class GatewaysTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \Admin\Model\Table\GatewaysTable
     */
    public $Gateways;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.Admin.Gateways',
        'plugin.Admin.Schedules',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Gateways') ? [] : ['className' => GatewaysTable::class];
        $this->Gateways = TableRegistry::getTableLocator()->get('Gateways', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Gateways);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
