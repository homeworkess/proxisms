<?php
namespace Admin\Test\TestCase\Model\Table;

use Admin\Model\Table\InvoiceLinesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * Admin\Model\Table\InvoiceLinesTable Test Case
 */
class InvoiceLinesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \Admin\Model\Table\InvoiceLinesTable
     */
    public $InvoiceLines;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.admin.invoice_lines',
        'plugin.admin.invoices',
        'plugin.admin.companies',
        'plugin.admin.users',
        'plugin.admin.accounts',
        'plugin.admin.cards',
        'plugin.admin.card_types',
        'plugin.admin.card_operations',
        'plugin.admin.employees',
        'plugin.admin.orders',
        'plugin.admin.order_lines',
        'plugin.admin.company_types',
        'plugin.admin.products'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('InvoiceLines') ? [] : ['className' => InvoiceLinesTable::class];
        $this->InvoiceLines = TableRegistry::get('InvoiceLines', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->InvoiceLines);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
