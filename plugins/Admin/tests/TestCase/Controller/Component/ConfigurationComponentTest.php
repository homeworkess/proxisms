<?php
namespace Admin\Test\TestCase\Controller\Component;

use Admin\Controller\Component\ConfigurationComponent;
use Cake\Controller\ComponentRegistry;
use Cake\TestSuite\TestCase;

/**
 * Admin\Controller\Component\ConfigurationComponent Test Case
 */
class ConfigurationComponentTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \Admin\Controller\Component\ConfigurationComponent
     */
    public $Configuration;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $registry = new ComponentRegistry();
        $this->Configuration = new ConfigurationComponent($registry);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Configuration);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
