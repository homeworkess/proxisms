<?php
namespace Admin\Test\TestCase\Form;

use Admin\Form\ContactsImportForm;
use Cake\TestSuite\TestCase;

/**
 * Admin\Form\ContactsImportForm Test Case
 */
class ContactsImportFormTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \Admin\Form\ContactsImportForm
     */
    public $ContactsImport;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $this->ContactsImport = new ContactsImportForm();
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ContactsImport);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
