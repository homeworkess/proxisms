<?php
namespace Admin\Test\TestCase\Form;

use Admin\Form\TwilioConfigForm;
use Cake\TestSuite\TestCase;

/**
 * Admin\Form\TwilioConfigForm Test Case
 */
class TwilioConfigFormTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \Admin\Form\TwilioConfigForm
     */
    public $TwilioConfig;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $this->TwilioConfig = new TwilioConfigForm();
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TwilioConfig);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
