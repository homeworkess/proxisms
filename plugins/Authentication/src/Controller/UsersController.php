<?php
namespace Authentication\Controller;

use Cake\Event\Event;

class UsersController extends AppController
{
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['login','logout']);
    }

    public function login()
    {
        if ($this->request->is('post')) {

            $user = $this->Auth->identify();

            if ($user) {

                $this->Auth->sessionKey ='Auth.Admin';
                $this->Auth->setUser($user);

                return $this->redirect([
                    'plugin' => 'Admin',
                    'controller' => 'Dashboard',
                    'action' => 'index'
                ]);

            }

            $this->Flash->error(__('Utilisateur ou mot de passe incorrect'));
        }

    }

    public function logOut(){
        $this->request->getSession()->destroy();
        return $this->redirect([
            'plugin' =>'Authentication',
            'controller' => 'Users',
            'action' => 'login'
        ]);
    }

}
