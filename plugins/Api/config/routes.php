<?php
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;
use Api\Middleware\BasicRemoverMiddleware;

Router::plugin('Api', ['path' => '/api'],

    function (RouteBuilder $routes) {

        $routes->scope('/', function ($routes) {

            //$routes->resources('Categories');
            $routes->resources('Transactions');
            $routes->resources('Users');
            $routes->resources('Customers');
            $routes->resources('Orders');
            $routes->resources('Catalogue');
            $routes->resources('Directory');
            $routes->resources('Restaurants');
            $routes->resources('Products');

            $routes->scope('/users/transactions', function ($routes) {

                $routes->registerMiddleware('BasicRemover', new BasicRemoverMiddleware());
                $routes->applyMiddleware('BasicRemover');

            });

            $routes->resources('Restaurants', function ($routes) {
                $routes->resources('Transactions');
            });

            $routes->resources('Categories', function ($routes) {
                $routes->resources('Products');
            });

        });

        $routes->fallbacks(DashedRoute::class);

    }

);
