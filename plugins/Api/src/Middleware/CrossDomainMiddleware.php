<?php
namespace Api\Middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * CrossDomain middleware
 */
class CrossDomainMiddleware
{

    /**
     * Invoke method.
     *
     * @param \Psr\Http\Message\ServerRequestInterface $request The request.
     * @param \Psr\Http\Message\ResponseInterface $response The response.
     * @param callable $next Callback to invoke the next middleware.
     * @return \Psr\Http\Message\ResponseInterface A response
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, $next)
    {

        if ($request->getMethod() == "OPTIONS") {

            $method   = $request->getHeader('Access-Control-Request-Method');
            $headers  = $request->getHeader('Access-Control-Request-Headers');
            $response = $response->withHeader('Access-Control-Allow-Headers', $headers);
            $response = $response->withHeader('Access-Control-Allow-Methods', empty($method) ? 'GET, POST, PUT, DELETE' : $method);
            $response = $response->withHeader('Access-Control-Allow-Credentials', 'true');
            $response = $response->withHeader('Access-Control-Max-Age', '120');
            $response = $response->withHeader('Access-Control-Allow-Origin', '*');
            $response->send();

            die();


        } else {


            $response = $response->withHeader('Access-Control-Allow-Origin', '*')
                ->withHeader('Access-Control-Allow-Credentials', 'true')
                ->withHeader('Access-Control-Max-Age', '86400');


        }


        $response = $next($request, $response);

        return $response;

        //return $next($request, $response);
    }
}


