<?php
namespace Api\Controller;

use Api\Controller\AppController;
use Cake\ORM\TableRegistry;

class CategoriesController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Auth', [
            'authenticate' => 'Api.Token',
            'storage' => 'Memory',
            'unauthorizedRedirect' => false
        ]);
        //$this->Auth->allow();
    }

    public function index(){

        $user = $this->Auth->user();

        $params = !is_null($user['customer']) ? ['ProductCategories.public_available' => 1] : ['ProductCategories.pro_available' => 1];

        $query = TableRegistry::getTableLocator()
            ->get('Api.ProductCategories')
            ->find('threaded')
            ->contain(['Products' => function($query){
                return $query->select('product_category_id');
            }])
            ->where(['active'=>true])
            ->where($params);

        return $this->response
            ->withStatus(200)
            ->withHeader('Content-Type','application/json')
            ->withStringBody(
                json_encode([
                    'results' => [
                        'success' => true,
                        'data'    => [
                            'categories' => $query->toArray()
                        ]
                    ]
                ])
            );

    }

    public function view($id){

        $query = TableRegistry::getTableLocator()
            ->get('Api.ProductCategories')
            ->get($id);

        /* Extraction des variantes */
        $children = TableRegistry::getTableLocator()
            ->get('Api.ProductCategories')
            ->find('children', ['for' => $id]);

        $query->children = $children;

        return $this->response
            ->withStatus(200)
            ->withHeader('Content-Type','application/json')
            ->withStringBody(
                json_encode([
                    'results' => [
                        'success' => true,
                        'data'    => [
                            'category' => $query
                        ]
                    ]
                ])
            );

    }



}
