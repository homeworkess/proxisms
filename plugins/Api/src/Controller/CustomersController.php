<?php
namespace Api\Controller;

use Api\Controller\AppController;
use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\TableRegistry;
use Cake\Utility\Security;
use Cake\Utility\Text;
use Cake\View\Helper\HtmlHelper;
use Cake\Routing\Router;

class CustomersController extends AppController
{

	private $resp =[
		'success' => false,
		'results'    => null
	];

    public function initialize()
    {
        parent::initialize();
    }

    /* Pour le tableau des commandes */
    public function index(){

        $draw   = (int)$this->request->getQuery('draw');
        $start  = (int)$this->request->getQuery('start');
        $length = (int)$this->request->getQuery('length');
        $level  = $this->request->getQuery('level');

        $query = $this->Customers
        ->find('all')
        ->contain(['Users'=>function($q){
            return $q->select(['active','created']);
         }])
        ->order(['Customers.id' => 'DESC']);

        if($length == -1){
            $query->limit(1844674407)->offset($start);

        } else {
           $query->limit($length)->offset($start);
        }

        $totalCount = $query->count();

        return $this->response
            ->withStatus(200)
            ->withHeader('Content-Type','application/json')
            ->withStringBody(
                json_encode([
                    "draw"=> $draw,
                    "start" => $start,
                    "length" => $length,
                    "recordsTotal"=> $totalCount,
                    "recordsFiltered"=> $totalCount,
                    'data' => $query->toArray()
                ])
            );

    }


    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add(){

        $data = $this->getRequest()->getData();

        $customerData = [
            'first_name' => $data['firstName'],
            'last_name' => $data['lastName'],
            'address' => $data['address'],
            'mobile' => $data['mobilePhone'],
            'user' => [
                'identity' => $data['mobilePhone'],
                'password' => $data['password'],
                'is_super_admin' => 0,
                'api_token' => Security::hash(Text::uuid())
            ]
        ];

        $customer = TableRegistry::getTableLocator()->get("Api.Customers")->newEntity($customerData);



        debug($customer);exit;

        if(TableRegistry::getTableLocator()

            ->get("Api.Customers")->save($customer)){

            return $this->response
                ->withStatus(200)
                ->withHeader('Content-Type','application/json')
                ->withStringBody(
                    json_encode([
                        'results' => [
                            'success' => true,
                            'data'    => [
                                'customer' => $customer
                            ]
                        ]
                    ])
                );

        } else {

            return $this->response
                ->withStatus(500)
                ->withHeader('Content-Type','application/json')
                ->withStringBody(
                    json_encode([
                        'results' => [
                            'success' => false,
                            'data'    => [
                                'errors' => $customer->getErrors()
                            ]
                        ]
                    ])
                );

        }

    }

    public function getCustomerByPhoneNumber(){

        $params = [];
        $mobile = $this->getRequest()->getQuery('mobile','');
        $query = TableRegistry::getTableLocator()->get('Api.Users');

        if( !empty($mobile) ){

            $params[] = ['Users.identity' => $mobile];

        }

        $user = $query->find('all')
            ->contain(['Customers'])
            ->where($params)
            ->first();

        /* Generate an API 'token' */
        $user->api_token = sha1(Text::uuid());
        TableRegistry::getTableLocator()->get('Api.Users')->save($user);

        return $this->response
            ->withStatus($user ? 200 : 404)
            ->withHeader('Content-Type','application/json')
            ->withStringBody(
                json_encode([
                    'results' => [
                        'success' => $user ? true : false,
                        'data'    => [
                            'user' => $user
                        ]
                    ]
                ])
            );

    }

        /**
     * Edit method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function edit()
    {

        if ($this->request->is('post')) {


        }

	    return $this->response
		    ->withStatus(200)
		    ->withHeader('Content-Type','application/json')
		    ->withStringBody(
			    json_encode($this->resp)
		    );

    }


}
