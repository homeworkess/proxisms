<?php
namespace Api\Controller;

use Api\Controller\AppController;
use Api\Form\RegistrationFormFactory;
use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\TableRegistry;
use Cake\Utility\Security;
use Cake\Utility\Text;

/**
 * Users Controller
 *
 *
 * @method \Api\Model\Entity\User[] paginate($object = null, array $settings = [])
 * @property bool|object Users
 */
class UsersController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Auth', [
            'authenticate' => [
                'Api.CustomBasic' => [
                   'fields' => ['username' => 'identity', 'password' => 'password'],
                   'userModel' => 'Users'
                ]
            ],
            'storage' => 'Memory',
            'unauthorizedRedirect' => false
        ]);

        $this->Auth->allow([
            'cardPinAuthenticate',
            'add',
            'view',
            'register',
            'authenticate',
            'resetPassword'
        ]);

    }

    /* @TODO REFACTORING VERIF CARD + PIN */
    public function cardPinAuthenticate(){

    	$user = $this->Users->find('all')
		    ->contain(['Employees'=>['Accounts'=>['Cards']]])
		    ->where([

		    	'Cards.serial_number' => $this->request->getData('card_serial')
		    ])
		    ->first();


    	/* CARTE INVALIDE */
    	if(!$user){

		    return $this->response
			    ->withStatus(200)
			    ->withHeader('Content-Type','application/json')
			    ->withStringBody(
				    json_encode([
					    'results' => [
						    'success' => false,
						    'message' => 'CARTE INVALIDE'
					    ]
				    ])
			    );

	    } else {


    		$isValid = (new DefaultPasswordHasher)->check( trim(strtoupper($this->request->getData('pin'))) , $user->password );

    		if($isValid){

			    /* CODE 200 */
			    return $this->response
				    ->withStatus(200)
				    ->withHeader('Content-Type','application/json')
				    ->withStringBody(
					    json_encode([
						    'results' => [
							    'success' => true,
							    'message' => 'CODE PIN VALID'
						    ]
					    ])
				    );

		    } else {

			    /* CODE 200 */
			    return $this->response
				    ->withStatus(200)
				    ->withHeader('Content-Type','application/json')
				    ->withStringBody(
					    json_encode([
						    'results' => [
							    'success' => false,
							    'message' => 'PIN INVALID'
						    ]
					    ])
				    );

		    }


	    }


    }

    public function authenticate()
    {
        $user = $this->Auth->identify();

        if ($user) {

            $user = $this->Users->get($user['id'], ['contain' => [
                        'Customers',
                        'Companies'
                        //'Employees'=>['Accounts' => 'Cards']
                    ]
                ]
            );

            /* Create a new token key */
            $passwordHelper = new DefaultPasswordHasher();

            /* Generate an API 'token' */
            $user->api_token = sha1(Text::uuid());

            /* Bcrypt the token so BasicAuthenticate can check it during login. */
            $user->api_token = $passwordHelper->hash($user->api_token);

            $this->Users->save($user);


            /* CODE 200 */
            return $this->response
                ->withStatus(200)
                ->withHeader('Content-Type','application/json')
                ->withStringBody(
                     json_encode([
                         'results' => [
                             'success' => true,
                             'data'    => [
                                 'user' => $user
                             ]
                         ]
                     ])
                );

        }

        /* CODE 401 */
        return $this->response->withStatus(401);


    }


    /**
     *  Use account registration
     * @param $data = array
     * @return \Cake\Http\Response|\Zend\Diactoros\MessageTrait|null
     */
    public function register(){

        $errors = [];
        $user = null;

        /* Customer or Restaurant */
        $validator = RegistrationFormFactory::build($this->getRequest()->getData('type'));

        if( $validator->execute($this->getRequest()->getData()) ){

            $user = $this->Users->register($this->getRequest()->getData());

            if($user->hasErrors()){
                $errors[] = $user->getErrors();
            }

        } else {

            $errors = $validator->getErrors();

        }

        return $this->response
            ->withStatus( empty($errors) ? 201 : 200)
            ->withHeader('Content-Type','application/json')
            ->withStringBody(
                json_encode([
                    'results' => [
                        'success' => empty($errors) ? true:false,
                        'data'    => [
                            'user' => empty($errors) ? $user:null,
                            'errors' => $errors
                        ]
                    ]
                ])
            );

    }

    public function resetPassword(){

        $password = $this->getRequest()->getQuery('password','vegetek2020');
        $token    = $this->getRequest()->getQuery('token');
        $user     = $this->Users->find('byToken',['token'=>h($token)]);

        if($user){

            $user->password = $password;
            $user->api_token = Security::hash(Text::uuid());
            $this->Users->save($user);

            return $this->response
                ->withStatus(200)
                ->withHeader('Content-Type','application/json')
                ->withStringBody(
                    json_encode([
                        'results' => [
                            'success' => true,
                            'data'    => [
                                'user' => $user
                            ]
                        ]
                    ])
                );

        } else {

            return $this->response
                ->withStatus(404)
                ->withHeader('Content-Type','application/json')
                ->withStringBody(
                    json_encode([
                        'results' => [
                            'success' => false,
                            'data'    => ['user' => $user],
                            'error' => 'Utilisateur introuvable'
                        ]
                    ])
                );

        }

    }

}
