<?php
namespace Api\Controller;

use Api\Common\ResponseDto;
use Api\Controller\AppController;
use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\TableRegistry;
use Cake\View\Helper\HtmlHelper;
use Cake\Routing\Router;
use PurchaseManager\Domain\Entity\Notification;
use PurchaseManager\Domain\Entity\NotificationError;
use PurchaseManager\Helpers\NotificationValidator;
use PurchaseManager\Helpers\UID;

/**
 * Orders Controller
 *
 *
 * @method \Api\Model\Entity\Order[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ProductsController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Auth', [
            'authenticate' => 'Api.Token',
            'storage' => 'Memory',
            'unauthorizedRedirect' => false
        ]);
        $this->Auth->allow();
    }

    public function index(){

        $user = $this->Auth->user();

        $params = is_null($user['customer']) ? ['public_available' => 1] : ['pro_available' => false];

        $categoryId = $this->request->getParam('category_id');

        if( !empty($categoryId) )
            $params[] = ['product_category_id' => $categoryId];

        $query = TableRegistry::getTableLocator()
            ->get('Api.Products')
            ->find('threaded')
            ->where(['active'=>true])
            ->where($params);

        return $this->response
            ->withStatus(200)
            ->withHeader('Content-Type','application/json')
            ->withStringBody(
                json_encode([
                    'results' => [
                        'success' => true,
                        'data'    => [
                            'products' => $query->toArray()
                        ]
                    ]
                ])
            );

    }

    public function view($id){

        $query = TableRegistry::getTableLocator()->get('Api.Products')
            ->get($id,['contain'=>['Units']]);

        $children = TableRegistry::getTableLocator()->get('Api.Products')
            ->find('children', ['for' => $id]);

        $query->children = $children;

        return $this->response
            ->withStatus(200)
            ->withHeader('Content-Type','application/json')
            ->withStringBody(
                json_encode([
                    'results' => [
                        'success' => true,
                        'data'    => [
                            'product' => $query
                        ]
                    ]
                ])
            );

    }

    public function getLatestProducts(){

        $query = TableRegistry::getTableLocator()->get('Api.Products')
            ->find('threaded')
            ->where(['active'=>true,'public_available'=>1]);

        return $this->response
            ->withStatus(200)
            ->withHeader('Content-Type','application/json')
            ->withStringBody(
                json_encode([
                    'results' => [
                        'success' => true,
                        'data'  => $query->toArray()
                    ]
                ])
            );

        //$this->set(['results' => $data->render(), '_serialize' => 'results',]);
        //$this->RequestHandler->renderAs($this, 'json');

    }

    public function getDiscountProducts(){

        $query = TableRegistry::getTableLocator()
            ->get('Api.Products')
            ->find('threaded')
            ->where(['active'=>true,'public_available' => 1, 'discount > 0'])
            ->select(['id','name','price', 'ref', 'discount','image','sequence'])
            ->orderAsc('Products.sequence')
            ->limit(10);

        return $this->response
            ->withStatus(200)
            ->withHeader('Content-Type','application/json')
            ->withStringBody(
                json_encode([
                    'results' => [
                        'success' => true,
                        'data'    => [
                            'assets_uri' => Router::url('/img/products/images/',true),
                            'products' => $query->toArray()
                        ]
                    ]
                ])
            );

    }

}
