<?php
namespace Api\Controller;

use Api\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\View\Helper\HtmlHelper;
use Cake\Routing\Router;

/**
 * Orders Controller
 *
 *
 * @method \Api\Model\Entity\Order[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class OrdersController extends AppController
{

	private $resp =[
		'success' => false,
		'results'    => null
	];

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Paginator');
        $this->loadComponent('Auth', [
            'authenticate' => [
                'Api.Token'
            ],
            'storage' => 'Memory',
            'unauthorizedRedirect' => false
        ]);

        $this->Auth->allow(['allOrders','getDefaultOrderListInJSON','add','getPublicOrders']);
    }

	public function index(){

        $user = $this->Auth->user();

        $params = ['Orders.origin' => 'vegetek'];

        /* Retrieve current user order's only */
        if( is_null($user['customer']) ){

            $params['Orders.company_id'] = $user['company']['id'];

            /* Check if the user is allowed to consult the history */
            //if(!$user['company']['account']['allow_delivery_details'])

        } else {
            $params['Orders.customer_id'] = $user['customer']['id'];
        }

        $orders = TableRegistry::getTableLocator()->get('Api.Orders')
            ->find('all')
            ->select(['Orders.id','Orders.delivery_date','Orders.status'])
            ->contain(['OrderLines' => ['Products','Units']])
            ->where($params)
            ->order(['Orders.created' =>'DESC'])
            ->toArray();

        return $this->response
            ->withStatus(200)
            ->withHeader('Content-Type','application/json')
            ->withStringBody(
                json_encode([
                    'results' => [
                        'success' => true,
                        'data'    => [
                            //'allow_delivery_price' => $companyAccount ? (bool)$companyAccount->account->allow_delivery_price : null,
                            //'allow_delivery_details' => $companyAccount ? (bool)$companyAccount->account->allow_delivery_details : null,
                            'orders' => $orders,
                        ]
                    ]
                ])
            );

    }

    /* Api pour l'impression des tickets */
    public function allOrders(){

        $orders = TableRegistry::getTableLocator()->get('Api.Orders')
            ->find('all')
            //->select(['Orders.id','Orders.delivery_date','Orders.status','Orders.origin','Orders.created'])
            ->contain(['OrderLines' => function($q) {
                return $q->contain(['Products','Units']);
            },"Companies"])
            ->where(['Orders.origin'=>'vegetek','Orders.deleted'=>false])
            ->order(['Orders.created'=>'DESC'])
            ->limit(300);

        /* Conversion */
        $ordersList  = [];

        foreach ($orders as $order) {

            $tempOrder = [
                "id" => $order->id,
                "delivery_date" => $order->delivery_date,
                "ref"=> $order->ref,
                "total_sales_cost"=> $order->totalSalesCost,
                "total_sales_cost_formated"=> number_format(sprintf('%05.2f', $order->totalSalesCost), 2, ',', ' '),
                "client_name" => $order->company->denomination
            ];

            /* Extraction des lignes produit */
            $orderLines = array_map(function($ol){

                return [
                    'product_name' => $ol->product->name,
                    'product_name_ar' => $ol->product->name_ar,
                    'qty_received' => is_null($ol->qty_received) ? '/':  sprintf('%06.3f', $ol->qty_received),
                    'sales_price' => sprintf('%06.2f',$ol->sales_price),
                    'unit_name' => $ol->unit->name,
                    'unit_abbr' => $ol->unit->abbr,
                ];

            },$order->order_lines);

            $tempOrder['order_count'] = count($orderLines);
            $tempOrder['order_lines'] = $orderLines;

            $ordersList[] = $tempOrder;

            unset($tempOrder);

        }

        return $this->response
            ->withStatus(200)
            ->withHeader('Content-Type','application/json')
            ->withStringBody(
                json_encode([
                    'results' => [
                        'success' => true,
                        'orders'  => $ordersList
                    ]
                ])
            );

    }

    /* Liste des commandes du grand publique */
    public function getPublicOrders(){

        $draw   = (int) $this->request->getQuery('draw');
        $start  = (int) $this->request->getQuery('start');
        $length = (int) $this->request->getQuery('length', -1);

        $conditions = ['Orders.origin'=>'vegetek_public'];

        /* Vérifier la présence de paramètres */
        if($length == -1){

            /* Filtrer le statut des commandes */
            if($this->request->getQuery('status') != ''){
                $conditions["Orders.status"] = $this->request->getQuery('status');
            }

            /* Filtrer par client */
            if($this->request->getQuery('customer_id') != ''){
                $conditions["Orders.customer_id"] = (int) $this->request->getQuery('customer_id');
            }

            /* Filtrer date de départ */
            if($this->request->getQuery('from') != ''){
                $conditions["Orders.created >="] = $this->request->getQuery('from');
            }

            /* Filtrer date de fin */
            if($this->request->getQuery('to') != ''){
                $conditions["Orders.created <="] = $this->request->getQuery('to');
            }

        }

        $orders = $this->Orders
            ->find('all')
            ->contain([
                'OrderLines'=> function($q){
                    return $q->select(['qty','sales_price','order_id']);
                } ,
                'Customers' => function($q){
                    return $q->select(['id','first_name','last_name','mobile']);
            }])
            ->where($conditions)
            ->order(['Orders.id' => 'DESC']);

        $orders->limit( $length == -1 ? 1844674407: $length )->offset($start);

        return $this->response
            ->withStatus(200)
            ->withHeader('Content-Type','application/json')
            ->withStringBody(
                json_encode([
                    "draw"=> $draw,
                    "start" => $start,
                    "length" => $length,
                    "recordsTotal"=> $orders->count(),
                    "recordsFiltered"=> $orders->count(),
                    'data' => $orders->toArray()
                ])
            );

    }

    /* Pour le tableau des commandes */
    public function getDefaultOrderListInJSON(){

        $draw   = (int)$this->request->getQuery('draw');
        $start  = (int)$this->request->getQuery('start');
        $length = (int)$this->request->getQuery('length');
        $level  = $this->request->getQuery('level');
        $origin = $this->request->getQuery('origin');

        $data = [];

        $conditions = ['Orders.origin'=>'vegetek','Orders.deleted'=>false];

        /* Vérifier la présence de paramètres */
        if($length == -1){

            /* Filtrer le statut des commandes */
            if($this->request->getQuery('status') != ''){
               $conditions["Orders.status"] = $this->request->getQuery('status');
            }

            /* Filtrer par client */
            if($this->request->getQuery('company_id') != ''){
               $conditions["Orders.company_id"] = $this->request->getQuery('company_id');
            }

            /* Filtrer date de départ */
            if($this->request->getQuery('from') != ''){
               $conditions["Orders.created >="] = $this->request->getQuery('from');
            }

            /* Filtrer date de fin */
            if($this->request->getQuery('to') != ''){
               $conditions["Orders.created <="] = $this->request->getQuery('to');
            }

        }

        $orders = null;

        if($origin == "public"){

            $conditions['Orders.origin'] = 'vegetek_public';

            $orders = $this->Orders
            ->find('all')
            ->contain(['OrderLines'=>['Products','Units'],'Customers'])
            ->where($conditions)
            ->order(['Orders.id' => 'DESC']);


        } else {

            $conditions['Orders.origin'] = 'vegetek';

            $orders = $this->Orders
            ->find('all')
            ->contain(['OrderLines'=>['Products','Units'],'Companies'])
            ->where($conditions)
            ->order(['Orders.id' => 'DESC']);

        }

        if($length == -1){
            $orders->limit(1844674407)->offset($start);

        } else {
           $orders->limit($length)->offset($start);
        }

        $ordersCount = $orders->count();

        $consultationLink = function($d){
            return '<a href="'.Router::url(['plugin'=>'PurchaseManager','controller'=>'orders', 'action'=>'view',$d->id]).'" >'
            .h(strtoupper($d->ref))
            .'</a>';
        };

        $statusLinks = function($d){

            /* TODO refactoring rendu sur la vue et non pas ici */
            $html = new HtmlHelper(new \Cake\View\View());

            $status = '';

            if($d->status == 'delivery'):
                $status = '<span class="label label-success label-rouded">LIVRAISON ENCOURS</span>';
            elseif($d->status == 'delivered'):
                $status = '<span class="label label-success label-rouded">LIVRÉ</span>';
            elseif($d->status == 'pending'):
                $status = '<span class="label label-warning label-rouded">EN ATTENTE</span>';
            elseif($d->status == 'valid'):
                $status = '<span class="label label-success label-rouded">Validé</span>';
            elseif($d->status == 'refused'):
                $status = '<span class="label label-warning label-rouded">Refusé</span>';
            elseif($d->status == 'canceled'):
                $status = '<span class="label label-danger label-rouded">Annulé</span>';
            endif;

            return $status;
        };

        foreach ($orders as $d) {

            $data[] = [
                $d->id,
                $consultationLink($d),
                $d->id,
                $origin == 'public' ? $d->customer->id : $d->company->id,
                h(strtoupper( is_null($d->company) ? $d->customer->first_name.', '.$d->customer->last_name : $d->company->denomination )),
                $d->status,
                $statusLinks($d),

                number_format((float)($d->totalPurchaseCost), 2, ".", " ")." DA",
                number_format((float)($d->totalSalesCost), 2, ".", " ")." DA",
                number_format((float)( $d->totalSalesCost > 0 ? $d->totalSalesCost - $d->totalPurchaseCost : 0 ), 2, ".", " ")." DA",

                h($d->created->i18nFormat('dd/MM/y HH:mm')),

                /* Créaction des liens des actions */
                (function($d,$level){

                    /* Rendu sur la vue */
                    $html = new HtmlHelper(new \Cake\View\View());

                    $links = '';

                    if($d->status == 'pending'):
                        $links .= "<a href=".Router::url(['plugin'=>'PurchaseManager','controller'=>'orders',  'action'=>'validate',$d->id]) ."  class='btn btn-success btn-sm  br-round'>VALIDER</a>";
                    endif;

                    if($d->status == 'delivery'):
                        $links .= "<a href=".Router::url(['plugin'=>'PurchaseManager','controller'=>'orders',  'action'=>'confirm-delivery',$d->id]) ."  class='btn btn-success btn-sm  br-round'>CONF. LIVRAISON</a>";
                    endif;

                    if($d->status != 'delivered' && $d->status != 'delivery' && $level === "manager" ):
                        $links .= "<a href=".Router::url(['plugin'=>'PurchaseManager','controller'=>'orders',  'action'=>'edit',$d->id]) ."  class='btn btn-wj btn-sm  br-round'>EDITER</a>";
                        $links .= $html->link('SUPPRIMER',['plugin'=>'PurchaseManager','controller'=>'orders','action'=>'delete',$d->id],['confirm'=>"êtes-vous sûr de vouloir supprimer la commande",'class'=>"btn btn-danger btn-sm  br-round"]);
                    endif;

                    if($d->status != 'canceled' && $d->status != 'delivered' && $level === "manager" ):
                        $links .= $html->link('ANNULER',['plugin'=>'PurchaseManager','controller'=>'orders','action'=>'process',$d->id,'canceled'],['confirm'=>"êtes-vous sûr de vouloir annuler la commande",'class'=>"btn btn-danger btn-sm  br-round"]);
                    endif;

                    if($d->status == 'delivered' && $level === "manager"  ):
                        $links .= $html->link('RÉINITIALISER',['plugin'=>'PurchaseManager','controller'=>'orders','action'=>'reset',$d->id],['confirm'=>"êtes-vous sûr de vouloir réinitialiser la commande",'class'=>"btn btn-danger btn-sm  br-round"]);
                        $links .= $html->link('SUPPRIMER',['plugin'=>'PurchaseManager','controller'=>'orders','action'=>'delete',$d->id],['confirm'=>"êtes-vous sûr de vouloir supprimer la commande",'class'=>"btn btn-danger btn-sm  br-round"]);
                    endif;

                    return $links;

                })($d,$level),
            ];

        }

        return $this->response
            ->withStatus(200)
            ->withHeader('Content-Type','application/json')
            ->withStringBody(
                json_encode([
                    "draw"=> $draw,
                    "start" => $start,
                    "length" => $length,
                    "recordsTotal"=> $ordersCount,
                    "recordsFiltered"=> $ordersCount,
                    'data' => $data
                ])
            );

    }

    public function add()
    {

        if ($this->request->is('post')) {

	        $order = $this->Orders->newEntity();

        	/* if user is a customer then extract products prices */
            if($this->request->getData('origin') === 'vegetek_public'){


                $pids = array_map(function($lineItem){return $lineItem['product_id'];}
                ,$this->request->getData('order_lines'));

                $productsPrices = TableRegistry::getTableLocator()->get('Api.products')
                    ->find('all')
                    ->where(['id IN('.implode(',',$pids).')'])
                    ->select(['id','price'])
                    ->disableHydration()
                    ->toArray();

                $getProductByID = function ($id) use ($productsPrices) {
                    foreach ($productsPrices as $pPrice) {
                        if ($pPrice['id'] === $id) {
                            return $pPrice['price'];
                        }

                    }
                    return 0;
                };

                /* Add unit price to Lines */
                $orderLines = array_map(function($line)use ($getProductByID){
                    return [
                        'product_id' => $line['product_id'],
                        'unit_id' => $line['unit_id'],
                        'qty' => $line['qty'],
                        'sales_price' => $getProductByID($line['product_id'])
                    ];
                },$this->request->getData('order_lines'));

                $orderData = $this->request->getData();
                $orderData['order_lines'] = $orderLines;

                $order = $this->Orders->patchEntity($order, $orderData);


            } else {

                $order = $this->Orders->patchEntity($order, $this->request->getData());

            }

            if ($this->Orders->save($order)) {

	            $this->resp['success'] = true;
	            $this->resp['results']['message'] = strtoupper('COMMANDE EFFECTUÉE');
	            $this->resp['results']['data'] = $order;

            } else {

	            $this->resp['success'] = false;
	            $this->resp['results']['message'] = strtoupper('ERREUR SERVEUR CONTACTEZ VOTRE ADMINISTRATEUR');
	            $this->resp['results']['errors'][] = $order->getErrors();

            }

        }

	    return $this->response
		    ->withStatus(200)
		    ->withHeader('Content-Type','application/json')
		    ->withStringBody(
			    json_encode($this->resp)
		    );

    }

}
