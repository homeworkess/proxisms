<?php
namespace Api\Controller;

use Api\Controller\AppController;
use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\TableRegistry;
use Cake\Utility\Text;

/**
 * Users Controller
 *
 *
 * @method \Api\Model\Entity\User[] paginate($object = null, array $settings = [])
 */
class RestaurantsController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        /*$this->loadComponent('Auth', [
            'authenticate' => [
                'Basic' => [
                    'fields' => ['username' => 'identity', 'password' => 'password'],
                    'userModel' => 'Users'
                ],
                'Api.DestyxRestaurantToken'
            ],
            'storage' => 'Memory',
            'unauthorizedRedirect' => false
        ]);

        $this->Auth->allow(['index','authenticate','edit','transactions']);*/
    }


    /* Récupération du token */
    public function authenticate()
    {

        $this->loadModel('Api.Users');

        $user = $this->Auth->identify();

        if ($user) {

            /* Récupération de l'utilisateur */
            $user = $this->Users->get(
                $user['id'], ['contain' => ['Companies'=>['Accounts']]]
            );


            /* Générer une clé pour l'API */
            if(is_null($user->api_token)){

                $hasher = new DefaultPasswordHasher();

                /* Generate an API 'token' */
                $user->api_token = sha1(Text::uuid());

                /* Bcrypt the token so BasicAuthenticate can check it during login. */
                $user->api_token = $hasher->hash($user->api_token);

                /* Persister l'utilisateur */
                $this->Users->save($user);

            }

            /* CODE 200 */
            return $this->response
                ->withStatus(200)
                ->withHeader('Content-Type','application/json')
                ->withStringBody(
                    json_encode([
                        'results' => [
                            'success' => true,
                            'data'    => [
                                'user' => $user
                            ]
                        ]
                    ])
                );

        }

        /* CODE d'3EEUR 41 */
        return $this->response
            ->withStatus(401)
            ->withStringBody(
                json_encode([
                    'results' => [
                        'success' => false,
                        'data'    => null
                    ]
                ])
            );


    }

    public function getCompanyByPhoneNumber(){

        $params = [];
        $mobile = $this->getRequest()->getQuery('mobile','');
        $query = TableRegistry::getTableLocator()->get('Api.Users');

        if( !empty($mobile) ){
            $params = ['Users.identity' => $mobile];
        }

        $user = $query->find('all')
            ->contain(['Companies'])
            ->where($params)
            ->first();

        return $this->response
            ->withStatus($user ? 200 : 404)
            ->withHeader('Content-Type','application/json')
            ->withStringBody(
                json_encode([
                    'results' => [
                        'success' => $user ? true : false,
                        'data'    => [
                            'user' => $user
                        ]
                    ]
                ])
            );

    }

}
