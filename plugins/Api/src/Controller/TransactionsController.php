<?php
namespace Api\Controller;

use Api\Controller\AppController;
use Api\Form\TransactionForm;
use Api\Services\TransactionService;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Core\Configure;
use Cake\Mailer\Email;
use Cake\ORM\TableRegistry;

/**
 * Transactions Controller
 *
 *
 * @method \Api\Model\Entity\Transaction[] paginate($object = null, array $settings = [])
 */
class TransactionsController extends AppController
{

    private $resp =[
        'success' => false,
        'results'    => null
    ];

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Auth', [
            'authenticate' => [
                'Basic' => [
                    'fields' => ['username' => 'identity', 'password' => 'password'],
                    'userModel' => 'Users'
                ],
                'Api.DestyxRestaurantToken'
            ],
            'storage' => 'Memory',
            'unauthorizedRedirect' => false
        ]);

        $this->Auth->allow(['add','index','executeTransaction']);

    }

    public function index()
    {

        $transactions = null;

        $user = $this->Auth->identify();

        if ($user && $restaurantId = $this->request->getParam('restaurant_id')) {

            $restaurant = TableRegistry::get('Api.Companies')->get($restaurantId);
            $transactions = TableRegistry::get('Api.transactions')
                ->find('all')
                ->select(['id', 'amount', 'created'])
                ->where([

                    'operation' => 'deposit',
                    'transaction_info' => 'restaurant_payment_deposit',
                    'account_id' => $restaurant->account_id

                ]);

            /* CODE 200 */
            return $this->response
                ->withStatus(200)
                ->withHeader('Content-Type','application/json')
                ->withStringBody(
                    json_encode([
                        'results' => [
                            'success' => true,
                            'data'    => [
                                'transactions' => $transactions
                            ]
                        ]
                    ])
                );

        }

        /* CODE d'3EEUR 41 */
        return $this->response
            ->withStatus(401)
            ->withStringBody(
                json_encode([
                    'results' => [
                        'success' => false,
                        'data'    => null
                    ]
                ])
            );



    }

    private function executeTransaction(){

        $this->autoRender = false;

        $amount = (float)$this->request->getData('amount');
        $companyUniqueId = $this->request->getData('uid');

        $difference = 0;

        /* Informations sur le restaurant */
        $company = TableRegistry::get('Api.Companies')
            ->find('all')
            ->where([ 'unique_id' => $companyUniqueId ])
            ->contain(['CompanyTypes','Accounts'=>['Cards'],'Users'])
            ->first();

        /* Compte employé */
        $employee = TableRegistry::get('Api.Employees')
            ->find('all')
            ->contain(['Users','Companies'=>['Accounts'], 'Accounts'=>['Cards']])
            ->where(['Cards.serial_number' =>  $this->request->getData('card_serial') ])
            ->first();

        /* Resto autorisé à utiliser les cartes ? */
        if( (!$company->account->allow_card_use) ){
            $this->resp['results']['errors'][] = [
                'restaurant_card_allowed' => [
                    'message' => strtoupper("Le restaurant n'est pas autorisé à utiliser les cartes")
                ]
            ];
            return false;
        }

        /* Compte Resto Active ? */
        if( (!$company->user->active) ){
            $this->resp['results']['errors'][] = [
                'account_disabled' => [
                    'message' => strtoupper("Compte du restaurant inactif")
                ]
            ];

            return false;
        }

        /* Salarié autorisé à utiliser une carte ? */
        if( (!$employee->account->allow_card_use) ){
            $this->resp['results']['errors'][] = [
                'card_not_allowed' => [
                    'message' => "vous n'est pas autorisé à utiliser les cartes"
                 ]
            ];
            return false;
        }

        /* Vérification du code PIN */
        if(
            !(new DefaultPasswordHasher)->check( trim($this->request->getData('pin')), $employee->user->password)
        ){
            $this->resp['results']['errors'][] = [
                'pin_error' => [
                    'message' => "CODE PIN INCORRECT"
                ]
            ];
            return false;
        }

        /* Carte Expiré */
        if( (!$employee->account->card->is_period_valid) ){
            $this->resp['results']['errors'][] = [
                'card_validity' => [
                    'message' => "La période de validité de votre Carte a expiré"
                ]
            ];
            return false;
        }

        /* PRENDRE EN CONSIDERATION LE CREDIT SHARE */
        /* Si Crédit == 0 ET Dépassement autorisé = 0 ET credit share non autorisé */
        if(
            ( (int)$employee->account->credit == 0 && (float)$employee->account->exceeding_allowed_credit == 0 && $employee->account->company_credit_link == 0 )
        ) {
            $this->resp['results']['errors'][] = [
                'credit_availaible' => [
                    'message' => "VOUS N'AVEZ PAS ASSEZ DE CREDIT"
                ]
            ];
            return false;
        }

        /* Commission & Discount */
        $restaurantDiscount   = $company->discount;
        $restaurantCommission = $company->commission;

        /* Le coût remisé */
        $costWithDiscount = function($cost) use($restaurantDiscount){
            if($restaurantDiscount > 0 )
                return ($cost - (($cost * $restaurantDiscount) / 100 ));
            return $cost;
        };

        $costWithDiscount = $costWithDiscount($amount);

        $costTotal = $amount - ( $amount *  (  $company->discount + $company->commission  )  / 100 );

        /* Retirer la somme du compte de l'employé */
        $employeeTransaction = TableRegistry::get('Api.Accounts')->get($employee->account->id);


        /* @TODO une partie du compte de l'employé et l'autre du compte de l'entreprise */

        /* PREMIER CAS RESERVE SUFFISANTE ET CREDIT SHARE NON AUTORISE */
        if
        (
            (int)$employee->account->credit >= $costWithDiscount
            &&
            (int)$employee->account->company_credit_link == 0

        )
        {
            $employeeTransaction->credit -= $costWithDiscount;

        }

        elseif ((

            /* LE PLAFOND PERMET DE FAIRE PASSER LA TRANSACTIONS */
            (int)$employee->account->credit == 0
            &&
            (float)$employee->account->exceeding_allowed_credit > 0
            &&
            ($employee->account->exceeding_allowed_credit - $employee->account->exceeding_consumption_credit ) >= $costWithDiscount

        ))
        {

            /* VERIFIER SI LENTREPRISE DISPOSE DE CREDIT DE DEPASSEMENT SUFFISAMENT, RETIRER DU COMPTE DE L'ENTREPRISE */
            if(( $employee->company->account->exceeding_allowed_credit - $employee->company->account->exceeding_consumption_credit ) >= $costWithDiscount ) {

                /* AJOUTER EXCEDANT AU COMPTE DE L'ENTREPRISE */
                $employee->company->account->exceeding_consumption_credit += $costWithDiscount;

                /* AJOUTER EXCEDANT AU COMPTE DE L'EMPLOYEE */
                $employeeTransaction->exceeding_consumption_credit += $costWithDiscount;

                /* RETIRER DU COMPTE DE L'ENTREPRISE */
                $employee->company->account->credit -= $costWithDiscount;

            } else {

                /* DEPASSEMENT RESERVE ENTREPRISE == 0 */
                $this->resp['results']['errors'][] = [
                    'reserve_disponible' => [
                        'message' => "L'ENTREPRISE A DEPASSE LE MONTANT DE LA RESERVE"
                    ]
                ];
                return false;

            }

        /* DEUXIEME CAS LE CREDIT COMPTE SUFFIT A PAYER */
        }

        /* CREDIT SHARE */
        elseif (

            (int)$employee->account->company_credit_link == 1

        ){

            if(

                /* SI L'ENTREPRISE DISPOSE DE CREDIT */
                ($employee->company->account->credit >= $costWithDiscount)

                ||

                /* SI FORFAIT DE L'ENTREPRISE TOUJOURS ACTIF */
                ( $employee->company->account->exceeding_allowed_credit - $employee->company->account->exceeding_consumption_credit ) >= $costWithDiscount


            ) {

                if(($employee->company->account->credit >= $costWithDiscount)){
                    /* RETIRER DU COMPTE DE L'ENTREPRISE */
                    $employee->company->account->credit -= $costWithDiscount;
                } else {
                    /* AJOUTER DEPASSEMENT */
                    $employee->company->account->exceeding_consumption_credit += $costWithDiscount;
                }

            } else {

                /* DEPASSEMENT RESERVE ENTREPRISE == 0 */
                $this->resp['results']['errors'][] = [
                    'reserve_disponible' => [
                        'message' => "L'ENTREPRISE A DEPASSE LE MONTANT DE LA RESERVE"
                    ]
                ];
                return false;

            }

        }

        elseif(

            (int)$employee->account->credit < $costWithDiscount

        ) {


            $this->resp['results']['errors'][] = [
                'reserve_disponible' => [
                    'message' => "VOUS AVEZ DEPASSE LE MONTANT DE LA RESERVE"
                ]
            ];
            return false;

        }

        /* Alimenter le compte du restaurant */
        $company->account->total_to_pay += $costTotal;

        $transactionMeta = [];

        /* Si présence de discount || commission ajouter les métadatas */
        if($restaurantDiscount > 0 || $restaurantCommission >0 ){
            $transactionMeta = [
                'commission' => $restaurantCommission,
                'discount'   => $restaurantDiscount,
            ];
        }

        /* 2- Passer la transaction retrait compte Salarié */
        $clientPaymentTransaction = TableRegistry::get('Api.Transactions')->newEntity([
            'user_id'    => $company->user->id, // Qui a executé la transaction
            'account_id' => $employee->account->id, // Sur quel Compte ??
            'amount'     => $costWithDiscount,//$costWithDiscount ,
            'difference' => $difference,
            'operation'  => 'withdraw',
            'transaction_meta' => $transactionMeta,
            'transaction_info' => 'employee_payment_withdraw'
        ]);

        /* Transaction Payment du restaurant */
        $restaurantPaymentTransaction = TableRegistry::get('Api.Transactions')->newEntity([
            'user_id'    => 1, // Qui a executé la transaction
            'account_id' => $company->account->id, // Sur quel Compte ??
            'amount'     => $costTotal ,
            'difference' => 0,
            'operation'  => 'deposit',
            'transaction_meta' => $transactionMeta,
            'transaction_info' => 'restaurant_payment_deposit'
        ]);

        /* Alimenter le compte 44 en commission */
        $desticomAccount = TableRegistry::get('Accounts')->get(44);
        $desticomAccount->commission_total += (float) (($amount * $company->commission)/100 ) ;

        /* Alimenter le compte de l'entreprise de l'employé en consommation WAPS */
        $employee->company->account->consumed_waps += (float)$costWithDiscount;

        /* Alimenter le compte de desticom en waps consommés */
        $desticomAccount->consumed_waps += (float)$costWithDiscount;

        /* DEMARRER UNE TRANSACTION */

        /* Retirer la somme du compte de l'employé & calculer éventuellement la monnaie */
        if(
            (TableRegistry::get('Api.Transactions')->save($restaurantPaymentTransaction))   // Persistence de la transaction Restaurant
            &&
            (TableRegistry::get('Api.Transactions')->save($clientPaymentTransaction))   // Persistence de la client
            &&
            (TableRegistry::get('Api.Accounts')->save($employeeTransaction)) // Mise à jour compte employé
            &&
            (TableRegistry::get('Api.Accounts')->save($company->account))    // Mise à jour compte Réstaurant
            &&
            (TableRegistry::get('Api.Accounts')->save($desticomAccount))     // Ajouter la commission
            &&
            (TableRegistry::get('Api.Accounts')->save($employee->company->account)) // Ajouter les waps consommés & eventuellement crédit dépassé
        ){

            /* Envoyer un mail pour l'historique */
            $msg = "Le compte de l'employé : ".$employee->first_name.', '.$employee->last_name." <br />".
                "A été débité de la somme de : ".$amount." <br />".
                "Le : ".(new \DateTime())->format('d-m-Y H:m');

//            $email = new Email('default');
//            $email
//                ->setTransport('gmail')
//                ->setFrom(['transactions@wajba-dz.com' ])
//                ->setTo(Configure::read('notification_email'))
//                ->setSubject("TRANSACTION"."-".$company->unique_id)
//                ->setEmailFormat('html')
//                ->send($msg);

            $this->resp['success'] = true;
            $this->resp['results']['message'] = strtoupper('Transaction Effectuée');
            $this->resp['results']['total_cost'] = $costWithDiscount;
            $this->resp['results']['discount']   = $restaurantDiscount;
            $this->resp['results']['difference'] = $difference;

            return true;

        } else {


            /* Annuler les transactions */
            $this->resp['results']['errors'][] = $restaurantPaymentTransaction->getErrors();
            $this->resp['results']['errors'][] = $clientPaymentTransaction->getErrors();
            $this->resp['results']['errors'][] = $employeeTransaction->getErrors();

            return false;

        }

    }

    public function add()
    {
        /* @TODO FIXER LE FORMULAIRE DE VALIDATION */
        $transactionForm = new TransactionForm();

        if($transactionForm->execute($this->request->getData()))
            $this->executeTransaction();
        else
            $this->resp['results']['errors'][] = $transactionForm->errors();

        return $this->response
            ->withStatus(200)
            ->withHeader('Content-Type','application/json')
            ->withStringBody(
                json_encode($this->resp)
            );

    }





}
