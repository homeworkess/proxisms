<?php

namespace Api\Controller;

use App\Controller\AppController as BaseController;
use Cake\Auth\DefaultPasswordHasher;

class AppController extends BaseController
{
    public function initialize() {
        parent::initialize();
        $this->loadComponent('RequestHandler');
    }

    public function beforeRender(\Cake\Event\Event $event)
    {
        //$this->RequestHandler->renderAs($this, 'json');
        $this->response->withType('application/json');
        $this->set('_serialize', true);
    }
}
