<?php
namespace Api\Controller;

use Api\Controller\AppController;
use Cake\ORM\TableRegistry;


class CatalogueController extends AppController
{

    public function index()
    {

        $defaultCategoriesImagesUri = "http://192.168.1.7/wajba/img/categories/images/";
        $defaultProductsImagesUri = "http://192.168.1.7/wajba/img/products/images/";

        $categories = TableRegistry::get('Api.ProductCategories')->find('all')->order(["sequence"=>"ASC"])->toArray();
        $products   = TableRegistry::get('Api.Products')->find('all')->contain(["Units","DefaultUnitEntity"])
            ->where(['active'=>true])
            ->order(["Products.sequence"=>"ASC"])
            ->toArray();


        return $this->response
            ->withStatus(200)
            ->withHeader('Content-Type','application/json')
            ->withStringBody(
                json_encode([
                    'results' => [
                        'success' => true,
                        'data'    => [

                            'category_uri' => $defaultCategoriesImagesUri,
                            'product_uri' => $defaultProductsImagesUri,
                            'categories' => $categories,
                            'products' => $products,
                            //'catalogue' => $catalogue,
                        ]
                    ]
                ])
            );
    }

}
