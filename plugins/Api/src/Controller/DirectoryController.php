<?php
namespace Api\Controller;

use Api\Controller\AppController;
use Cake\ORM\TableRegistry;
use App\Controller\AppController as BaseController;

/**
 * Directory Controller
 *
 *
 * @method \Api\Model\Entity\Directory[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DirectoryController extends BaseController
{

    public function initialize() {

        parent::initialize();
        $this->loadComponent('Auth', [

            'authenticate' => [
                'Api.DestyxRestaurantToken',
            ],

            'unauthorizedRedirect' => false,
            'authError' => 'Did you really think you are allowed to see that ?',
            'storage' => 'Memory'

        ]);

        $this->Auth->allow(['index']);


    }


    /**
     * Récupération de la liste des employés
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {

        $user = $this->Auth->identify();

        if($user) {

            $this->loadModel('Api.Employees');

            $employees = $this->Employees
                ->find('employeesInfos')
                ->enableHydration(false);

            //$user['company']['id']

            $transactions = TableRegistry::get('Api.Transactions')
                ->find('all')
                ->enableHydration(false)
                ->toArray();

            return $this->response
                ->withStatus(200)
                ->withHeader('Content-Type','application/json')
                ->withStringBody(
                    json_encode([
                        'results' => [
                            'success' => true,
                            'data'    => [
                                'employees' => $employees,
                                'transactions' => $transactions,
                            ]
                        ]
                    ])
                );

        }

       /* CODE d'EREUR 401 */
        return $this->response
            ->withStatus(401);


    }



}
