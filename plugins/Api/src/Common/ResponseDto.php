<?php


namespace Api\Common;


class ResponseDto
{
    private $response = [
                'success' => true,
                'data' => []
    ];

    public function __construct()
    {
        return $this;
    }

    public function setReponseData(bool $sucess= true, $data){
        $this->response['success'] = $sucess;
        $this->response['data'] = $data;
        return $this;
    }

    public function getResponseData(){
        return $this->response;
    }

    public function render(){
        return $this->response;
    }

}
