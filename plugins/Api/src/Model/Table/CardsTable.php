<?php
namespace Api\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Cards Model
 *
 * @property \Api\Model\Table\CardTypesTable|\Cake\ORM\Association\BelongsTo $CardTypes
 * @property \Api\Model\Table\AccountsTable|\Cake\ORM\Association\HasMany $Accounts
 * @property \Api\Model\Table\CardOperationsTable|\Cake\ORM\Association\HasMany $CardOperations
 *
 * @method \Api\Model\Entity\Card get($primaryKey, $options = [])
 * @method \Api\Model\Entity\Card newEntity($data = null, array $options = [])
 * @method \Api\Model\Entity\Card[] newEntities(array $data, array $options = [])
 * @method \Api\Model\Entity\Card|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Api\Model\Entity\Card patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Api\Model\Entity\Card[] patchEntities($entities, array $data, array $options = [])
 * @method \Api\Model\Entity\Card findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class CardsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('cards');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('CardTypes', [
            'foreignKey' => 'card_type_id',
            'joinType' => 'INNER',
            'className' => 'Api.CardTypes'
        ]);
        $this->hasMany('Accounts', [
            'foreignKey' => 'card_id',
            'className' => 'Api.Accounts'
        ]);
        $this->hasMany('CardOperations', [
            'foreignKey' => 'card_id',
            'className' => 'Api.CardOperations'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->dateTime('use_start')
            ->requirePresence('use_start', 'create')
            ->notEmpty('use_start');

        $validator
            ->dateTime('expiration')
            ->requirePresence('expiration', 'create')
            ->notEmpty('expiration');

        $validator
            ->scalar('serial_number')
            ->requirePresence('serial_number', 'create')
            ->notEmpty('serial_number');

        $validator
            ->requirePresence('active', 'create')
            ->notEmpty('active');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['card_type_id'], 'CardTypes'));

        return $rules;
    }
}
