<?php
namespace Api\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Companies Model
 *
 * @property \Api\Model\Table\UniquesTable|\Cake\ORM\Association\BelongsTo $Uniques
 * @property \Api\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 * @property \Api\Model\Table\AccountsTable|\Cake\ORM\Association\BelongsTo $Accounts
 * @property \Api\Model\Table\EmployeesTable|\Cake\ORM\Association\HasMany $Employees
 * @property \Api\Model\Table\OrdersTable|\Cake\ORM\Association\HasMany $Orders
 *
 * @method \Api\Model\Entity\Company get($primaryKey, $options = [])
 * @method \Api\Model\Entity\Company newEntity($data = null, array $options = [])
 * @method \Api\Model\Entity\Company[] newEntities(array $data, array $options = [])
 * @method \Api\Model\Entity\Company|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Api\Model\Entity\Company patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Api\Model\Entity\Company[] patchEntities($entities, array $data, array $options = [])
 * @method \Api\Model\Entity\Company findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class CompaniesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('companies');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Uniques', [
            'foreignKey' => 'unique_id',
            'joinType' => 'INNER',
            'className' => 'Api.Uniques'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'LEFT',
            'className' => 'Api.Users'
        ]);
        $this->belongsTo('Accounts', [
            'foreignKey' => 'account_id',
            'joinType' => 'LEFT',
            'className' => 'Api.Accounts'
        ]);
        $this->hasMany('Employees', [
            'foreignKey' => 'company_id',
            'className' => 'Api.Employees'
        ]);
        $this->hasMany('Orders', [
            'foreignKey' => 'company_id',
            'className' => 'Api.Orders'
        ]);
        $this->belongsTo('CompanyTypes', [
            'foreignKey' => 'company_type_id',
            'joinType' => 'INNER',
            'className' => 'Api.CompanyTypes'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('denomination')
            ->requirePresence('denomination', 'create')
            ->notEmpty('denomination');

        $validator
            ->scalar('address')
            ->requirePresence('address', 'create')
            ->notEmpty('address');

        $validator
            ->scalar('city')
            ->allowEmpty('city');

        $validator
            ->scalar('rc_n')
            ->allowEmpty('rc_n');

        $validator
            ->scalar('iden_fiscal')
            ->allowEmpty('iden_fiscal');

        $validator
            ->scalar('phone')
            ->allowEmpty('phone');

        $validator
            ->scalar('mobile')
            ->allowEmpty('mobile');

        $validator
            ->integer('uid')
            ->allowEmpty('uid');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        //$rules->add($rules->existsIn(['unique_id'], 'Uniques'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['account_id'], 'Accounts'));
        return $rules;
    }
}
