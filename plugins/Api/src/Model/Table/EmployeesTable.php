<?php
namespace Api\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Employees Model
 *
 * @property \Api\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 * @property \Api\Model\Table\CompaniesTable|\Cake\ORM\Association\BelongsTo $Companies
 * @property \Api\Model\Table\AccountsTable|\Cake\ORM\Association\BelongsTo $Accounts
 *
 * @method \Api\Model\Entity\Employee get($primaryKey, $options = [])
 * @method \Api\Model\Entity\Employee newEntity($data = null, array $options = [])
 * @method \Api\Model\Entity\Employee[] newEntities(array $data, array $options = [])
 * @method \Api\Model\Entity\Employee|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Api\Model\Entity\Employee patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Api\Model\Entity\Employee[] patchEntities($entities, array $data, array $options = [])
 * @method \Api\Model\Entity\Employee findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class EmployeesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('employees');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
            'className' => 'Api.Users'
        ]);

        $this->belongsTo('Companies', [
            'foreignKey' => 'company_id',
            'joinType' => 'INNER',
            'className' => 'Api.Companies'
        ]);

        $this->belongsTo('Accounts', [
            'foreignKey' => 'account_id',
            'joinType' => 'INNER',
            'className' => 'Api.Accounts'
        ]);



    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('first_name')
            ->requirePresence('first_name', 'create')
            ->notEmpty('first_name');

        $validator
            ->scalar('last_name')
            ->requirePresence('last_name', 'create')
            ->notEmpty('last_name');

        $validator
            ->scalar('address')
            ->allowEmpty('address');

        $validator
            ->email('email')
            ->allowEmpty('email');

        $validator
            ->scalar('mobile')
            ->allowEmpty('mobile');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['company_id'], 'Companies'));
        $rules->add($rules->existsIn(['account_id'], 'Accounts'));

        return $rules;
    }




    public function findEmployeesInfos(\Cake\ORM\Query $query, array $options)
    {
        return $query->contain([

            'Accounts' =>
                [
                    'fields' =>
                        [
                            'id',
                            'credit',
                            'allow_card_use',
                            'company_credit_link',
                            'exceeding_allowed_credit',
                            'exceeding_consumption_credit',
                        ],
                    'Cards' =>
                        [
                            'fields' =>
                                [
                                    'id',
                                    'use_start',
                                    'expiration',
                                    'serial_number',
                                ]
                        ]
                ],

            'Users' =>
                [
                    'fields' =>
                        [
                            'id',
                            'identity',
                            'identity',
                            'password',
                            'active',
                        ]
                ],

            'Companies' =>
                [
                    'fields' =>
                    [
                        'id',
                        'unique_id',
                        'denomination',
                        'Accounts.credit',
                    ],
                    'Accounts' =>
                        [
                            'fields' =>
                                [
                                    'allow_card_use',
                                    'company_credit_link',
                                    'exceeding_allowed_credit',
                                    'exceeding_consumption_credit',
                                ]
                        ]
                ]

        ])->select(['id','first_name','last_name','mobile']);

    }



}
