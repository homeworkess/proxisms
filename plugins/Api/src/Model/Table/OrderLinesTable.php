<?php
namespace Api\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * OrderLines Model
 *
 * @property \Api\Model\Table\ProductsTable|\Cake\ORM\Association\BelongsTo $Products
 * @property \Api\Model\Table\OrdersTable|\Cake\ORM\Association\BelongsTo $Orders
 *
 * @method \Api\Model\Entity\OrderLine get($primaryKey, $options = [])
 * @method \Api\Model\Entity\OrderLine newEntity($data = null, array $options = [])
 * @method \Api\Model\Entity\OrderLine[] newEntities(array $data, array $options = [])
 * @method \Api\Model\Entity\OrderLine|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Api\Model\Entity\OrderLine patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Api\Model\Entity\OrderLine[] patchEntities($entities, array $data, array $options = [])
 * @method \Api\Model\Entity\OrderLine findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class OrderLinesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('order_lines');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Products', [
            'foreignKey' => 'product_id',
            'className' => 'Api.Products'
        ]);
        $this->belongsTo('Orders', [
            'foreignKey' => 'order_id',
            'className' => 'Api.Orders'
        ]);
        $this->belongsTo('Units', [
            'foreignKey' => 'unit_id',
            'className' => 'Api.Units'
        ]);

    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->numeric('qty')
            ->allowEmpty('qty');

        $validator
            ->numeric('qty_received')
            ->allowEmpty('qty_received');

        $validator
            ->integer('tickets_per_stack')
            ->allowEmpty('tickets_per_stack');

        $validator
            ->numeric('facial_value')
            ->allowEmpty('facial_value');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['product_id'], 'Products'));
        $rules->add($rules->existsIn(['order_id'], 'Orders'));

        return $rules;
    }
}
