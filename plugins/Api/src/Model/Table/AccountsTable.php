<?php
namespace Api\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Accounts Model
 *
 * @property \Api\Model\Table\CardsTable|\Cake\ORM\Association\BelongsTo $Cards
 * @property \Api\Model\Table\CompaniesTable|\Cake\ORM\Association\HasMany $Companies
 * @property \Api\Model\Table\EmployeesTable|\Cake\ORM\Association\HasMany $Employees
 * @property \Api\Model\Table\TransactionsTable|\Cake\ORM\Association\HasMany $Transactions
 *
 * @method \Api\Model\Entity\Account get($primaryKey, $options = [])
 * @method \Api\Model\Entity\Account newEntity($data = null, array $options = [])
 * @method \Api\Model\Entity\Account[] newEntities(array $data, array $options = [])
 * @method \Api\Model\Entity\Account|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Api\Model\Entity\Account patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Api\Model\Entity\Account[] patchEntities($entities, array $data, array $options = [])
 * @method \Api\Model\Entity\Account findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class AccountsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('accounts');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Cards', [
            'foreignKey' => 'card_id',
            'className' => 'Api.Cards'
        ]);
        $this->hasMany('Companies', [
            'foreignKey' => 'account_id',
            'className' => 'Api.Companies'
        ]);
        $this->hasMany('Employees', [
            'foreignKey' => 'account_id',
            'className' => 'Api.Employees'
        ]);
        $this->hasMany('Transactions', [
            'foreignKey' => 'account_id',
            'className' => 'Api.Transactions'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('credit')
            ->requirePresence('credit', 'create')
            ->notEmpty('credit');

        $validator
            ->requirePresence('allow_card_use', 'create')
            ->notEmpty('allow_card_use');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['card_id'], 'Cards'));
        return $rules;
    }

    public function withdraw(){}

}
