<?php
namespace Api\Model\Table;

use Cake\Event\Event;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Utility\Security;
use Cake\Utility\Text;
use Cake\Validation\Validator;

/**
 * Users Model
 *
 * @property \Api\Model\Table\CompaniesTable|\Cake\ORM\Association\HasMany $Companies
 * @property \Api\Model\Table\EmployeesTable|\Cake\ORM\Association\HasMany $Employees
 * @property \Api\Model\Table\TransactionsTable|\Cake\ORM\Association\HasMany $Transactions
 *
 * @method \Api\Model\Entity\User get($primaryKey, $options = [])
 * @method \Api\Model\Entity\User newEntity($data = null, array $options = [])
 * @method \Api\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \Api\Model\Entity\User|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Api\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Api\Model\Entity\User[] patchEntities($entities, array $data, array $options = [])
 * @method \Api\Model\Entity\User findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UsersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('users');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasOne('Companies', [
            'foreignKey' => 'user_id',
            'className' => 'Api.Companies',
            'joinType' => 'left'
        ]);

        $this->hasOne('Customers', [
            'foreignKey' => 'user_id',
            'className' => 'Api.Customers',
            'joinType' => 'left'
        ]);

        $this->hasOne('Employees', [
            'foreignKey' => 'user_id',
            'className' => 'Api.Employees'
        ]);

        $this->hasMany('Transactions', [
            'foreignKey' => 'user_id',
            'className' => 'Api.Transactions'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('identity')
            ->maxLength('identity', 100)
            ->requirePresence('identity', 'create')
            ->notEmpty('identity');

        $validator
            ->scalar('password')
            ->maxLength('password', 255)
            ->requirePresence('password', 'create')
            ->notEmpty('password');

        $validator
            ->scalar('recovery_token')
            ->maxLength('recovery_token', 255)
            ->allowEmpty('recovery_token');

        $validator
            ->scalar('api_token')
            ->maxLength('api_token', 255)
            ->allowEmpty('api_token');

        $validator
            ->boolean('is_super_admin')
            ->requirePresence('is_super_admin', 'create')
            ->notEmpty('is_super_admin');

        return $validator;
    }


    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['identity'],'Ce numéro existe déjà sur notre système'));
        return $rules;
    }

    public function beforeMarshal($event,$data,$options)
    {
        /* if new account set mobile as username */
        if(!isset($data['user']['id'])){
            if(isset($data['customer']))
                $data['identity'] = $data['customer']['mobile'];

            //$data['api_token'] = Text::uuid();
        }
        return $data;
    }

    /**
     * Retrieve a user by token
     * @param $query Object
     * @param $param Array
     * @return mixed
     */
    public function findByToken($query, $param){
        return $query->where(['Users.api_token'=>$param['token']])
            ->contain(['Companies','Customers'])
            ->first();
    }

    /**
     * Register a user account Restaurant or customer
     * @param $param Array
     * @return Entity
     */
    public function register($param){

        $user = null;

        if($param['type'] === 'customer'){

            $user = $this->newEntity([
                'is_super_admin' => 0,
                'identity' => isset($param['mobilePhone']) ? $param['mobilePhone'] : '',
                'password' => isset($param['password']) ? $param['password'] : '',
                'api_token' => Security::hash(Text::uuid()),
                'customer' => [
                    'first_name' => isset($param['firstName']) ? $param['firstName'] : '',
                    'last_name' => isset($param['lastName']) ? $param['lastName'] : '',
                    'mobile' => isset($param['mobilePhone']) ? $param['mobilePhone'] : '',
                    'address' => isset($param['address']) ? $param['address'] : '',
                ]
            ]);

        } else if($param['type'] === 'restaurant') {

            $user = $this->newEntity([
                'is_super_admin' => 0,
                'identity' => isset($param['mobilePhone']) ? $param['mobilePhone'] : '',
                'password' => isset($param['password']) ? $param['password'] : '',
                'api_token' => Security::hash(Text::uuid()),
                'company' => [
                    'company_type_id' => 2,
                    'denomination' => isset($param['companyName']) ? $param['companyName'] : '',
                    'mobile' => isset($param['mobilePhone']) ? $param['mobilePhone'] : '',
                    'address' => isset($param['address']) ? $param['address'] : '',
                    'account' => [
                        'credit' => 0,
                        'total_to_pay' => 0,
                        'commission_total' => 0,
                        'ordered_waps' => 0,
                        'consumed_waps' => 0,
                        'allow_card_use' => 0,
                        'allow_purchase' => 0,
                        'allow_delivery_price' => 0,
                        'allow_delivery_details' => 0,
                        'notifications_threshold' => 0,
                        'company_credit_link' => 0,
                        'exceeding_allowed_credit' => 0,
                        'exceeding_consumption_credit' => 0,
                    ]
                ]
            ],['associated' => ['Companies.Accounts']]);

        }

        $this->save($user);
        return $user;
    }



}
