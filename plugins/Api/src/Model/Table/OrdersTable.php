<?php
namespace Api\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Orders Model
 *
 * @property \Api\Model\Table\CompaniesTable|\Cake\ORM\Association\BelongsTo $Companies
 * @property \Api\Model\Table\OrderLinesTable|\Cake\ORM\Association\HasMany $OrderLines
 *
 * @method \Api\Model\Entity\Order get($primaryKey, $options = [])
 * @method \Api\Model\Entity\Order newEntity($data = null, array $options = [])
 * @method \Api\Model\Entity\Order[] newEntities(array $data, array $options = [])
 * @method \Api\Model\Entity\Order|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Api\Model\Entity\Order patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Api\Model\Entity\Order[] patchEntities($entities, array $data, array $options = [])
 * @method \Api\Model\Entity\Order findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class OrdersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('orders');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Companies', [
            'foreignKey' => 'company_id',
            'joinType' => 'LEFT',
            'className' => 'Api.Companies'
        ]);

        $this->belongsTo('Customers', [
            'foreignKey' => 'customer_id',
            'joinType' => 'LEFT',
            'className' => 'Api.Customers'
        ]);

        $this->hasMany('OrderLines', [
            'foreignKey' => 'order_id',
            'className' => 'Api.OrderLines'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->date('delivery_date')
            ->requirePresence('delivery_date', 'create')
            ->notEmpty('delivery_date');

        $validator
            ->scalar('origin')
            ->maxLength('origin', 50)
            ->allowEmpty('origin');

        $validator
            ->scalar('status')
            ->requirePresence('status', 'create')
            ->notEmpty('status');

        $validator
            ->scalar('remarks')
            ->maxLength('remarks', 500)
            ->allowEmpty('remarks');

        $validator
            ->allowEmpty('uid');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['company_id'], 'Companies'));
        return $rules;
    }


	public function beforeMarshal($event,$data,$options)
	{
		$data['status']            = 'pending';
		return true;
	}





}
