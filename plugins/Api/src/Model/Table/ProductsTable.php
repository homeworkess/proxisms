<?php
namespace Api\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Products Model
 *
 * @property \Api\Model\Table\ProductCategoriesTable|\Cake\ORM\Association\BelongsTo $ProductCategories
 * @property \Api\Model\Table\InvoiceLinesTable|\Cake\ORM\Association\HasMany $InvoiceLines
 * @property \Api\Model\Table\OrderLinesTable|\Cake\ORM\Association\HasMany $OrderLines
 *
 * @method \Api\Model\Entity\Product get($primaryKey, $options = [])
 * @method \Api\Model\Entity\Product newEntity($data = null, array $options = [])
 * @method \Api\Model\Entity\Product[] newEntities(array $data, array $options = [])
 * @method \Api\Model\Entity\Product|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Api\Model\Entity\Product patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Api\Model\Entity\Product[] patchEntities($entities, array $data, array $options = [])
 * @method \Api\Model\Entity\Product findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ProductsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('products');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->addBehavior('Tree', [
            'recoverOrder' => ['sequence' => 'ASC'],
        ]);

        $this->belongsTo('ProductCategories', [
            'foreignKey' => 'product_category_id',
            'joinType' => 'INNER',
            'className' => 'Api.ProductCategories'
        ]);

        $this->hasMany('InvoiceLines', [
            'foreignKey' => 'product_id',
            'className' => 'Api.InvoiceLines'
        ]);

        $this->hasMany('OrderLines', [
            'foreignKey' => 'product_id',
            'className' => 'Api.OrderLines'
        ]);

	    $this->belongsToMany('Units', [
		    'joinTable' => 'products_units',
	    ]);

        $this->belongsTo('DefaultUnitEntity', [
            'foreignKey' => 'default_unit',
            'joinTable' => 'units',
            'className' => 'Api.Units'
        ]);

    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->scalar('name_ar')
            ->maxLength('name_ar', 255)
            ->allowEmpty('name_ar');

        $validator
            ->scalar('ref')
            ->maxLength('ref', 255)
            ->allowEmpty('ref');

        $validator
            ->numeric('price')
            ->requirePresence('price', 'create')
            ->notEmpty('price');

        $validator
            ->scalar('image')
            ->maxLength('image', 255)
            ->allowEmpty('image');

        $validator
            ->scalar('description')
            ->maxLength('description', 1000)
            ->allowEmpty('description');

        $validator
            ->boolean('active')
            ->requirePresence('active', 'create')
            ->notEmpty('active');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['product_category_id'], 'ProductCategories'));
        return $rules;
    }

    public function findLatestProducts($query){

        return $query->find('threaded')
            ->where(['active'=>true,'public_available'=>1])
            ->select(['id','name','price','discount','image','sequence','description'])
            ->limit(10);

    }


}
