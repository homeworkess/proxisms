<?php
namespace Api\Model\Entity;

use Cake\ORM\Entity;

/**
 * Order Entity
 *
 * @property int $id
 * @property \Cake\I18n\FrozenDate $delivery_date
 * @property int $company_id
 * @property string $origin
 * @property string $status
 * @property string $remarks
 * @property int $uid
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \Api\Model\Entity\Company $company
 * @property \Api\Model\Entity\OrderLine[] $order_lines
 */
class Order extends Entity
{
    protected $_accessible = [
        '*' => true,
    ];

    protected $_hidden = ['uid','remarks','delivery_address'];

    protected $_virtual = ['totalPurchaseCost','totalSalesCost','ref'];

    protected function _getRef(){

        if(isset($this->_properties['created']) && !is_null($this->_properties['created'])){
            return 'BC-'.$this->_properties['created']->i18nformat('yy').'/'.$this->_properties['id'];
        }

        return "-";
    }


    protected function _getTotalPurchaseCost(){

        if(is_null($this->order_lines))
            return 0;

        return number_format(array_reduce($this->order_lines, function($carry, $item){
            return $carry + (float)  ($item->qty_received * $item->purchase_price);
        }),2, '.', '');

    }

    protected function _getTotalSalesCost(){

        if(empty($this->order_lines))
            return 0;

        return number_format(array_reduce($this->order_lines, function($carry, $item){

            if( $this->origin === 'vegetek_public' ){

                return $carry + (float) ($item->qty * $item->sales_price);

            } else {

                return $carry + (float) ($item->qty_received * $item->sales_price);

            }

        }),2, '.', '');

    }

}
