<?php
namespace Api\Model\Entity;

use Cake\ORM\Entity;
use Cake\Routing\Router;

/**
 * ProductCategory Entity
 *
 * @property int $id
 * @property string $name
 * @property int $product_category_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \Api\Model\Entity\ProductCategory[] $product_categories
 * @property \Api\Model\Entity\Product[] $products
 */
class ProductCategory extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'product_category_id' => true,
        'created' => true,
        'modified' => true,
        'product_categories' => true,
        'products' => true
    ];

    protected $_hidden = [
      'created','modified'
    ];

    protected $_virtual = ['thumbnail','total_products'];

    public function _getTotalProducts(){
        return isset($this->_properties['products']) ? count($this->_properties['products']) : '';
    }

    public function _getThumbnail(){
	    return  Router::url('/img/categories/images/thumbnail-'.$this->_properties['image'],true);
    }

	public function _getImage(){
		return  Router::url('/img/categories/images/'.$this->_properties['image'],true);
	}

}
