<?php
namespace Api\Model\Entity;

use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Entity;

/**
 * User Entity
 *
 * @property int $id
 * @property string $identity
 * @property string $pin_code
 * @property string $password
 * @property string $recovery_token
 * @property string $api_token
 * @property bool $is_super_admin
 * @property bool $active
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \Api\Model\Entity\Company[] $companies
 * @property \Api\Model\Entity\Employee[] $employees
 * @property \Api\Model\Entity\Transaction[] $transactions
 */
class User extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = ['*' => true];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = ['password','modified'];

    protected function _setPassword($password){
        return (new DefaultPasswordHasher())->hash($password);
    }

}
