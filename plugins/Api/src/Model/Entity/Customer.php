<?php
namespace Api\Model\Entity;

use Cake\ORM\Entity;

class Customer extends Entity
{

    protected $_accessible = [
        '*' => true,
    ];

    protected $_virtual = ['fullName'];
    protected $_hidden = ['user_id'];

    protected function _getFullName(){
        return isset($this->_properties['first_name']) && isset($this->_properties['last_name']) ?
            $this->_properties['first_name'].', '.$this->_properties['last_name'] : '';
    }

}
