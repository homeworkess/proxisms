<?php
namespace Api\Model\Entity;

use Cake\ORM\Entity;

/**
 * TransactionMeta Entity
 *
 * @property int $id
 * @property int $transaction_id
 * @property float $commission
 * @property float $discount
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \Api\Model\Entity\Transaction $transaction
 */
class TransactionMeta extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'transaction_id' => true,
        'commission' => true,
        'discount' => true,
        'created' => true,
        'modified' => true,
        'transaction' => true
    ];
}
