<?php
namespace Api\Model\Entity;

use Cake\ORM\Entity;

/**
 * Company Entity
 *
 * @property int $id
 * @property string $unique_id
 * @property string $denomination
 * @property string $type
 * @property string $address
 * @property string $city
 * @property string $rc_n
 * @property string $iden_fiscal
 * @property string $phone
 * @property string $mobile
 * @property int $user_id
 * @property int $account_id
 * @property int $uid
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \Api\Model\Entity\Unique $unique
 * @property \Api\Model\Entity\User $user
 * @property \Api\Model\Entity\Account $account
 * @property \Api\Model\Entity\Employee[] $employees
 * @property \Api\Model\Entity\Order[] $orders
 */
class Company extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true
    ];
}
