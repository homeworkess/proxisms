<?php
namespace Api\Model\Entity;

use Cake\Chronos\MutableDateTime;
use Cake\ORM\Entity;

/**
 * Card Entity
 *
 * @property int $id
 * @property \Cake\I18n\FrozenTime $use_start
 * @property \Cake\I18n\FrozenTime $expiration
 * @property int $card_type_id
 * @property string $serial_number
 * @property int $active
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \Api\Model\Entity\CardType $card_type
 * @property \Api\Model\Entity\Account[] $accounts
 * @property \Api\Model\Entity\CardOperation[] $card_operations
 */
class Card extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'use_start' => true,
        'expiration' => true,
        'card_type_id' => true,
        'serial_number' => true,
        'active' => true,
        'created' => true,
        'modified' => true,
        'card_type' => true,
        'accounts' => true,
        'card_operations' => true
    ];

    protected $_hidden = [
        'created',
        'modified',
        "card_type_id"
    ];


    protected function _getIsPeriodValid()
    {
        $now = new MutableDateTime('now');
        $start =  $this->_properties['use_start'];
        $expiration = $this->_properties['expiration'];

        return  ($now->gte($start) && $now->lt($expiration));
    }



}
