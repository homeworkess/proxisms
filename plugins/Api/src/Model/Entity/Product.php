<?php
namespace Api\Model\Entity;

use Cake\ORM\Entity;
use Cake\Routing\Router;


/**
 * Product Entity
 *
 * @property int $id
 * @property string $name
 * @property string $name_ar
 * @property string $ref
 * @property float $price
 * @property string $image
 * @property string $description
 * @property int $product_category_id
 * @property bool $active
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \Api\Model\Entity\ProductCategory $product_category
 * @property \Api\Model\Entity\InvoiceLine[] $invoice_lines
 * @property \Api\Model\Entity\OrderLine[] $order_lines
 */
class Product extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'name_ar' => true,
        'ref' => true,
        'price' => true,
        'image' => true,
        'description' => true,
        'product_category_id' => true,
        'active' => true,
        'created' => true,
        'modified' => true,
        'product_category' => true,
        'invoice_lines' => true,
        'order_lines' => true
    ];

    protected $_hidden = [
        'created','modified'
    ];

	protected $_virtual = ['thumbnail'];

	public function _getThumbnail(){
        return  isset($this->_properties['image']) ? 'thumbnail-'.$this->_properties['image']  : '';
	}

    public function _getImage(){
        return  isset($this->_properties['image']) ? $this->_properties['image'] : '';
    }

}
