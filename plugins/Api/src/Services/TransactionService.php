<?php
/**
 * Created by PhpStorm.
 * User: Ryadh
 * Date: 15/11/2017
 * Time: 13:03
 */

namespace Api\Services;


use Cake\ORM\TableRegistry;

class TransactionService
{



    public function withdraw($command){

        $response = [];

        /* 1- Vérifier la validité du Compte Employé ( Carte + Crédit > 0 ) */
        $company = TableRegistry::get('Api.Companies')
            ->find('all')
            ->where([ 'unique_id' => $command['rest_uid'],'type' => 'restaurant' ])
            ->contain(['Accounts'=>['Cards'],'Users'])
            ->first();

        /* Resto autorisé à utiliser les cartes ? */
        if( (!$company->account->allow_card_use) ){

            $response['errors'][] = [
                'message' => "Le restaurant n'est pas autorisé à utiliser les cartes"
            ];

            return $response;
        }


        /* Compte Resto Active ? */
        if( (!$company->user->active) ){

            $response['errors'][] = [
                'message' => "Compte désactivé"
            ];

            return $response;
        }

        $employee = TableRegistry::get('Api.Employees')
            ->find('all')
            ->contain(['Accounts'=>['Cards']])
            ->where(['Cards.serial_number' => $command['card_serial'] ])
            ->first();



        /* Salarié autorisé à utiliser une carte ? */
        if( (!$employee->account->allow_card_use) ){

            $response['errors'][] = [
                'message' => "vous n'est pas autorisé à utiliser les cartes"
            ];

            return $response;
        }

        /* Carte Expiré */
        if( (!$employee->account->card->is_period_valid) ){

            $response['errors'][] = [
                'message' => "La période de validité de votre Carte a expiré"
            ];

            return $response;
        }


        /* 2- Passer la transaction */
        $transaction = TableRegistry::get('Api.Transactions')->newEntity([
            'user_id'    => $company->user->id, // Qui a executé la transaction
            'account_id' => $employee->account->id, // Sur quel Compte ??
            'amount'     => $command['amount'] ,
            'difference' => 0,
            'operation'  => 'withdraw',
        ]);

        if(TableRegistry::get('Api.Transactions')->save($transaction)){
            $response['success'] = true;
            $response['message'] = 'Transaction Effectuée';
            return $response;

        } else {

            $response['success'] = false;
            $response['errors'][] = $transaction->getErrors();

            return $response;


        }


    }



}