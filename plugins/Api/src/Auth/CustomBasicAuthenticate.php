<?php


namespace Api\Auth;

use Cake\Auth\BaseAuthenticate;
use Cake\Auth\BasicAuthenticate;
use Cake\Auth\PasswordHasherFactory;
use Cake\Http\ServerRequest;
use Cake\Http\Response;
use Cake\ORM\TableRegistry;
use Cake\Auth\DefaultPasswordHasher;

class CustomBasicAuthenticate extends BasicAuthenticate
{
    public function authenticate(ServerRequest $request, Response $response)
    {
        /* Retirer le header du basic machin */
        return parent::authenticate($request,$response);
    }

    public function unauthenticated(ServerRequest $request, Response $response)
    {

    }


}
