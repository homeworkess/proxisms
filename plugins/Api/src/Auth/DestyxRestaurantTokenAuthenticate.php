<?php


namespace Api\Auth;

use Cake\Auth\BaseAuthenticate;
use Cake\Auth\BasicAuthenticate;
use Cake\Auth\PasswordHasherFactory;
use Cake\Http\ServerRequest;
use Cake\Http\Response;
use Cake\ORM\TableRegistry;
use Cake\Auth\DefaultPasswordHasher;

class DestyxRestaurantTokenAuthenticate extends BaseAuthenticate
{
    public function authenticate(ServerRequest $request, Response $response)
    {
        if( count($request->getHeader('Authorization')) === 0 )
            return false;

        if (preg_match('/Bearer\s(\S+)/', $request->getHeader('Authorization')[0], $token))
           return TableRegistry::get('Api.Users')
                ->find('All')
               ->contain(['Companies'])
               ->where(['Users.api_token'=>$token[1],'Companies.company_type_id'=>2])
               ->enableHydration(false)
               ->first();

        return false;

    }

    public function unauthenticated(ServerRequest $request, Response $response)
    {
        parent::unauthenticated($request,$response->withStatus(401));
        return $response->withStatus(401);
    }

}
