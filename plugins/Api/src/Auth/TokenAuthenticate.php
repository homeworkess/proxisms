<?php


namespace Api\Auth;

use Cake\Auth\BaseAuthenticate;
use Cake\Http\ServerRequest;
use Cake\Http\Response;
use Cake\ORM\TableRegistry;

class TokenAuthenticate extends BaseAuthenticate
{

    public function authenticate(ServerRequest $request, Response $response)
    {

        if( count($request->getHeader('Authorization')) === 0 )
            return false;

        if (preg_match('/Bearer\s(\S+)/', $request->getHeader('Authorization')[0], $token))
           return TableRegistry::get('Api.Users')
                ->find('All')
               ->contain(['Companies'])
               ->where(['Users.api_token'=>$token[1],'Companies.company_type_id'=>2])
               ->enableHydration(false)
               ->first();

        return false;

    }

    public function getUser(ServerRequest $request)
    {
        if( count($request->getHeader('Authorization')) === 0 )
            parent::getUser($request);

        if(isset($request->getHeader('Authorization')[0])) {

            if (preg_match('/Bearer\s(\S+)/', $request->getHeader('Authorization')[0], $token)){

                $user = TableRegistry::get('Api.Users')
                    ->find('All')
                    ->contain(['Companies', 'Customers'])
                    ->where(['Users.api_token' => $token[1] ])
                    ->enableHydration(false)
                    ->first();

                return $user;

            }

        }

    }



    public function unauthenticated(ServerRequest $request, Response $response)
    {
        parent::unauthenticated($request,$response->withStatus(401));
        return $response->withStatus(401);
    }

}
