<?php
namespace Api\Form;

use Cake\Form\Form;
use Cake\Form\Schema;
use Cake\Validation\Validator;

class CustomerRegistrationForm extends Form
{
    /**
     * Builds the schema for the modelless form
     *
     * @param \Cake\Form\Schema $schema From schema
     * @return \Cake\Form\Schema
     */
    protected function _buildSchema(Schema $schema)
    {
        return $schema;
    }

    /**
     * Form validation builder
     *
     * @param \Cake\Validation\Validator $validator to use against the form
     * @return \Cake\Validation\Validator
     */
    protected function _buildValidator(Validator $validator)
    {
        return $validator->scalar('firstName')
            ->requirePresence('firstName',true,'Nom de téléphone obligatoire')

            /* Last name */
            ->scalar('lastName')
            ->requirePresence('lastName',true,'Prénom de téléphone obligatoire')

            /* Mobile Phone number */
            ->numeric('mobilePhone')
            ->requirePresence('mobilePhone',true,'Numéro de téléphone obligatoire')

            /* Address */
            ->scalar('address')
            ->allowEmptyString('address')

            /* Password */
            ->scalar('password')
            ->minLength('password',6,'Mot de passe obligatoire, minimum 6 caractères')
            ->requirePresence('password');
    }

    /**
     * Defines what to execute once the Form is processed
     *
     * @param array $data Form data.
     * @return bool
     */
    protected function _execute(array $data)
    {
        return true;
    }
}
