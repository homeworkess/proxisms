<?php
namespace Api\Form;

use Cake\Form\Form;
use Cake\Form\Schema;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

/**
 * Transaction Form.
 */
class TransactionForm extends Form
{
    /**
     * Builds the schema for the modelless form
     *
     * @param \Cake\Form\Schema $schema From schema
     * @return \Cake\Form\Schema
     */
    protected function _buildSchema(Schema $schema)
    {
        return $schema;
    }

    /**
     * Form validation builder
     *
     * @param \Cake\Validation\Validator $validator to use against the form
     * @return \Cake\Validation\Validator
     */
    protected function _buildValidator(Validator $validator)
    {

        $cardsTable = TableRegistry::get('Cards');
        $restaurantTable = TableRegistry::get('Companies');

        return $validator

            /* Validation de l'existence et de la validité d'une carte */
            ->add('card_serial', [
                'message' => [
                    'rule' => function ($value, $context) use ($cardsTable) {
                        $card = $cardsTable->find('all')->where(['serial_number' => $value ]);
                        return (bool)$card->count();
                    },
                    'last' => true,
                    'message' => 'CARTE INVALIDE'
                ]
            ])

            /* Vérifier la présence du Restaurant & l'autorisation à effectuer des transactions */
            ->add('uid', 'message', [
                'rule' => function ($value, $context) use ($restaurantTable) {
                    $restaurant = $restaurantTable
                        ->find('all')
                        ->where(['unique_id' => $value ]);
                    return (bool)$restaurant->count();
                },
                'last' => true,
                'message' => 'RESTAURANT INVALID'
            ]);


    }

    protected function _execute(array $data)
    {
        /* Envoyer un Message SMS */
        return true;
    }
}
