<?php


namespace Api\Form;


class RegistrationFormFactory
{
    public static function build(string $type){
        return $type === 'customer' ? new CustomerRegistrationForm()
            : new RestaurantRegistrationForm();
    }

}
