<?php
namespace Api\Test\TestCase\Model\Table;

use Api\Model\Table\OrderLinesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * Api\Model\Table\OrderLinesTable Test Case
 */
class OrderLinesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \Api\Model\Table\OrderLinesTable
     */
    public $OrderLines;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.api.order_lines',
        'plugin.api.products',
        'plugin.api.product_categories',
        'plugin.api.invoice_lines',
        'plugin.api.orders',
        'plugin.api.companies',
        'plugin.api.uniques',
        'plugin.api.users',
        'plugin.api.employees',
        'plugin.api.accounts',
        'plugin.api.cards',
        'plugin.api.card_types',
        'plugin.api.card_operations',
        'plugin.api.transactions',
        'plugin.api.transaction_metas',
        'plugin.api.company_types'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('OrderLines') ? [] : ['className' => OrderLinesTable::class];
        $this->OrderLines = TableRegistry::get('OrderLines', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->OrderLines);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
