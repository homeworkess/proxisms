<?php
namespace Api\Test\TestCase\Model\Table;

use Api\Model\Table\CardsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * Api\Model\Table\CardsTable Test Case
 */
class CardsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \Api\Model\Table\CardsTable
     */
    public $Cards;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.api.cards',
        'plugin.api.card_types',
        'plugin.api.accounts',
        'plugin.api.companies',
        'plugin.api.uniques',
        'plugin.api.users',
        'plugin.api.employees',
        'plugin.api.orders',
        'plugin.api.transactions',
        'plugin.api.card_operations'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Cards') ? [] : ['className' => CardsTable::class];
        $this->Cards = TableRegistry::get('Cards', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Cards);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
