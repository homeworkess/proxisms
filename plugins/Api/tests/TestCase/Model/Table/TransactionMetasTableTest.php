<?php
namespace Api\Test\TestCase\Model\Table;

use Api\Model\Table\TransactionMetasTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * Api\Model\Table\TransactionMetasTable Test Case
 */
class TransactionMetasTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \Api\Model\Table\TransactionMetasTable
     */
    public $TransactionMetas;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.api.transaction_metas',
        'plugin.api.transactions',
        'plugin.api.accounts',
        'plugin.api.cards',
        'plugin.api.card_types',
        'plugin.api.card_operations',
        'plugin.api.companies',
        'plugin.api.uniques',
        'plugin.api.users',
        'plugin.api.employees',
        'plugin.api.orders',
        'plugin.api.company_types'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('TransactionMetas') ? [] : ['className' => TransactionMetasTable::class];
        $this->TransactionMetas = TableRegistry::get('TransactionMetas', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TransactionMetas);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
