<?php
namespace Api\Test\TestCase\Form;

use Api\Form\TransactionForm;
use Cake\TestSuite\TestCase;

/**
 * Api\Form\TransactionForm Test Case
 */
class TransactionFormTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \Api\Form\TransactionForm
     */
    public $Transaction;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $this->Transaction = new TransactionForm();
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Transaction);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
