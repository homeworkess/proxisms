<?php
namespace Api\Test\TestCase\Form;

use Api\Form\RestaurantRegistrationForm;
use Cake\TestSuite\TestCase;

/**
 * Api\Form\RestaurantRegistrationForm Test Case
 */
class RestaurantRegistrationFormTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \Api\Form\RestaurantRegistrationForm
     */
    public $RestaurantRegistration;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $this->RestaurantRegistration = new RestaurantRegistrationForm();
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->RestaurantRegistration);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
