<?php
namespace Api\Test\TestCase\Form;

use Api\Form\CustomerRegistrationForm;
use Cake\TestSuite\TestCase;

/**
 * Api\Form\CustomerRegistrationForm Test Case
 */
class CustomerRegistrationFormTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \Api\Form\CustomerRegistrationForm
     */
    public $CustomerRegistration;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $this->CustomerRegistration = new CustomerRegistrationForm();
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CustomerRegistration);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
