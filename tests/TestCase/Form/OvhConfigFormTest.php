<?php
namespace App\Test\TestCase\Form;

use App\Form\OvhConfigForm;
use Cake\TestSuite\TestCase;

/**
 * App\Form\OvhConfigForm Test Case
 */
class OvhConfigFormTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Form\OvhConfigForm
     */
    public $OvhConfig;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $this->OvhConfig = new OvhConfigForm();
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->OvhConfig);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
