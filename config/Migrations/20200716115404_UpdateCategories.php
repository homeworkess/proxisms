<?php
use Migrations\AbstractMigration;

class UpdateCategories extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {

        $table = $this->table("product_categories")

            ->addColumn('parent_id', 'integer', [
                'limit' => 11,
                'null' => true,
            ])

            ->addColumn('lft', 'integer', [
                'limit' => 11,
                'null' => false,
            ])

            ->addColumn('rght', 'integer', [
                'limit' => 11,
                'null' => false,
            ])

            ->addColumn('pro_available', 'boolean', [
                'default' => false,
                'null' => true,
            ]);

        $table->update();


    }
}
