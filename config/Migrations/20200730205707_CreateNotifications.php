<?php
use Migrations\AbstractMigration;

class CreateNotifications extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('notifications');

        $table->addColumn('title','string',['null'=>false]);

        $table->addColumn('created','datetime',['null'=>false]);
        $table->addColumn('modified','datetime',['null'=>true]);

        /* Type can be order status related like canceling, or a more generic like discount */
        $table->addColumn('type','integer',['null'=>false]);

        /* Default status 0 => pending */
        $table->addColumn('status','integer',['null'=>false,'default'=>0]);

        $table->create();
    }
}
